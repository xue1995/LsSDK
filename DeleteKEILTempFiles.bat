@echo off
::del *.zip /s
::del *.rar /s
del *.xml  /s
del *.crun  /s
del *.cspy.bat  /s
del *.dni  /s
del *.jlink /s
del *.scvd /s
del *.SBR /s
::del JLinkSettings.ini /s

del *.bak /s
del *.orig /s
del *.ddk /s
del *.edk /s
del *.lst /s
del *.lnp /s
del *.mpf /s
del *.mpj /s
del *.obj /s
del *.omf /s
::del *.opt /s  ::不允许删除JLINK的设置
del *.plg /s
del *.rpt /s
del *.tmp /s
del *.__i /s
del *.crf /s
del *.o /s
del *.d /s
del *.axf /s
del *.tra /s
del *.dep /s
del *.lnp /s
del JLinkLog.txt /s

del *.iex /s
del *.htm /s
del *.sct /s
del *.map /s
::del *.hex /s
::del *.uvoptx /s
::del *.uvguix /s
del *.pbi /s
del *.pbi.cout /s
del *.out /s
del *.cout /s
del *.browse /s
del *.pbd /s
del *.TMP /s
::del *.uvguix.* /s
del *.uvmpw.uvgui.* /s
del *.scc /s
del *.sim /s
del *.i /s
del *.pbi.xcl /s
del *.pbd.linf /s
del *.wsdt /s
del *.dbgdt /s
del *.wspos /s
del *.Debug.general.xcl /s
del *.Debug.driver.xcl /s
del *.pyc /s
rd /s/q .\tool\xAudioTool\build\ /s
rd /s/q .\tool\xAudioTool\dist\ /s
rd /s/q .\tool\xAudioTool\__pycache__\ /s
rd /s/q .\tool\xBin2Dfu\build\ /s
rd /s/q .\tool\xBin2Dful\dist\ /s
rd /s/q .\tool\xBin2Dfu\__pycache__\ /s
pause
exit
