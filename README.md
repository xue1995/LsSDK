# LsSDK介绍
打造一个通用性MCU架构，支持CX32L003等。<br>
OS支持RTX4/RTX5/FreeRtos。<br>
采用VsCode+KEIL5，超强开发方式。<br>
QQ群：524408033<br>

# 软件架构
```
+---app
|   +---app_adc
|   +---app_key
|   +---app_led
|   +---app_power
|   +---app_soft_voltameter
|   \---app_temp_control
+---bsp
|   \---cx32l003
+---chip
|   \---CX32L003_SDK
+---drivers
|   +---g_sensor
|   \---oled
+---lib
|   +---crc16
|   +---queue
|   +---str_hex
|   +---kv_sys
|   \---x_strtok
+---os
|   +---freertos
|   +---freertos_nrf_rtc
|   +---rtx
|   \---rtx5
+---platform
|   +---at_comm
|   |   \---cx32f0
|   +---log
|   \---SEGGER_RTT
+---project
|   \---jlink_burn
+---protocol
|   +---uart_dfu
|   +---uart_nrf_ble
|   \---usb_comm_ugee
+---sys
|   +---at32f4xx
|   +---cx32f0
|   +---nrf51
|   \---nrf52
\---tool
    +---128x64取模
    +---LiSunTool
    +---nrf_tool
    +---xBin2Dfu
    \---xMerge
```

![](tool/ls_syscfg_gui/界面.gif)
![](tool/ls_syscfg_gui/代码.gif)

![](tool/ls_debug_tool/界面.gif)


