/********************************************************************************
* @file    app_io.c
* @author  jianqiang.xue
* @version V1.1.0
* @date    2023-02-26
* @brief   IO功能
* @note    1. 支持IO、ADC、PWM
********************************************************************************/
/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "os_api.h"
#include "log.h"
#include "kv_sys.h"
#include "atcmd_slave.h"
/* Private Includes ----------------------------------------------------------*/
#include "app_main.h"
#include "ls_syscfg.h"
#include "ls_key.h"
#define LS_IO
#include "ls_gpio.h"
#include "ls_led.h"

#include "app_io.h"
/* Private Define ------------------------------------------------------------*/

/* Typedef Struct ------------------------------------------------------------*/

/* Public Enum ---------------------------------------------------------------*/
/* External Variables --------------------------------------------------------*/
#if LS_APP_IO_TIMER_SUPPORT
extern void app_io_task_init(void);
#endif

#if LS_ADC0_EN
extern uint8_t app_adc0_collection_init(void);
#endif

#if LS_ADC1_EN
extern uint8_t app_adc1_collection_init(void);
#endif

/* Public Variables ----------------------------------------------------------*/
/* Private Variables ---------------------------------------------------------*/
ls_io_t g_io = {
    .out = io_out,
    .in = get_io_ste,
};

/****************软定时器创建****************/
/* Private Function Prototypes -----------------------------------------------*/
void signal_irq(void) {
    //io_out(LS_IO_LED_IND_ID, 1);
}

void clean_signal_irq(void) {
    //io_out(LS_IO_LED_IND_ID, 1);
    // for (uint8_t i = 1; i < LS_IO_NUM; i++)
    //     g_io.func[i].io_event = 0;
}



/* Public Function Prototypes ------------------------------------------------*/
/**
 * @brief  从FLASH读取boot信息配置
 */
bool flash_read_app_io_all(void) {
    uint8_t* p = kv_get_env(LS_KV_KEY_IO_INFO);
    if (p != NULL) {
        // memcpy((uint8_t*)&g_io, p, sizeof(ls_io_t));
        return true;
    } else {
        // for (uint8_t i = 1; i < LS_IO_NUM; i++) {
        //     g_io.type[i] = g_io_cfg[i].type;
        // }
        // g_io.flag.save_support          = LS_APP_IO_SAVE_SUPPORT;
        // g_io.flag.active_report_support = LS_APP_IO_ACTIVE_REPORT_SUPPORT;
        // g_io.flag.key_support           = LS_APP_IO_KEY_SCAN_SUPPORT;
        // g_io.flag.matrix_key_support    = LS_APP_IO_MATRIX_KEY_SUPPORT;

        // g_io.flag.timer_support         = LS_APP_IO_TIMER_SUPPORT;

        // g_io.flag.sys_vcc_support       = LS_APP_IO_SYS_VCC_SUPPORT;
        // g_io.flag.swo_off               = LS_APP_IO_SWO_OFF;
        return false;
    }
}

/**
 * @brief  写配置信息到FLASH
 */
bool flash_write_app_io_all(void) {
    if (g_io.flag.save_support) {
        if (g_io.flag.write_in) {
            g_io.flag.write_in = 0;
            kv_set_env(LS_KV_KEY_IO_INFO, (uint8_t*)&g_io, sizeof(ls_io_t));
        }
        return true;
    }
    return false;
}

void app_io_init(void) {
    uint32_t timer_ret = 0;
#if LS_APP_IO_TIMER_SUPPORT
    app_io_task_init();
#endif
#if LS_ADC0_EN
    app_adc0_collection_init();
#endif
#if LS_ADC1_EN
    app_adc1_collection_init();
#endif
    bsp_gpio_exti_irq_reg_cb(app_io_exti_cb);
    // memset(&g_io, 0, sizeof(ls_io_t));
    // flash_read_app_io_all();
}


__WEAK void key_event(key_event_t event) {
    signal_irq();
}

/* ATCMD Function Prototypes ------------------------------------------------*/
#if ATCMD_EN
#endif
