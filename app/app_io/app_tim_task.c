/********************************************************************************
* @file    app_tim_task.c
* @author  jianqiang.xue
* @version V1.1.0
* @date    2023-03-21
* @brief   PWM周期任务
********************************************************************************/
/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "os_api.h"
#include "log.h"
#include "kv_sys.h"
#include "atcmd_slave.h"
/* Private Includes ----------------------------------------------------------*/
#include "app_main.h"
#include "ls_gpio.h"
#include "ls_syscfg.h"
#include "app_io.h"

/* Private Define ------------------------------------------------------------*/

/* Typedef Struct ------------------------------------------------------------*/
/* Public Enum ---------------------------------------------------------------*/
/* External Variables --------------------------------------------------------*/
/* Public Variables ----------------------------------------------------------*/
/* Private Variables ---------------------------------------------------------*/
#if LS_APP_PWM_TIMER_SUPPORT
io_task_t *g_pwm_task = NULL;
uint8_t g_pwm_task_num = 0;
uint32_t g_timer_pwm_time_ms = LS_APP_PWM_TIMER_TIME_MS; // 软件定时器的基准时间
#endif
/****************软定时器创建****************/


/* Private Function Prototypes -----------------------------------------------*/
/* Public Function Prototypes ------------------------------------------------*/

#if ATCMD_EN
#if LS_TIM1_EN || LS_TIM2_EN
#include "bsp_pwm.h"
#include "bsp_tim.h"
#if LS_TIM1_EN

extern bsp_tim_cfg_t g_tim1_cfg;
static int atcmd_get_tim1_info(atcmd_pack_t *pack) {
    char buff[70] = {0};
    uint8_t len = 0;
    len = snprintf(buff, 70, "+TIM1_INFO:%u,%u,%u,%u,%u,%u,%u\r\n",
           g_tim1_cfg.prescaler, g_tim1_cfg.period, g_tim1_cfg.level_logic,
           bsp_pwm_get_pulse(0), bsp_pwm_get_pulse(1), bsp_pwm_get_pulse(2), bsp_pwm_get_pulse(3));
    pack->reply((uint8_t *)buff, len);
    return 0;
}

static int atcmd_set_tim1_cfg(atcmd_pack_t *pack) {
    uint32_t argc[3] = {0};
    char buff[25] = {0};
    uint8_t len = 0;
    pack->argc = sscanf((char*)(pack->data), "%u,%u,%u", &argc[0], &argc[1], &argc[2]);
    if (pack->argc != 3) {
        len = snprintf(buff, 25, "+TIM1_CFG:ARGCERR\r\n");
        pack->reply((uint8_t*)buff, len);
        return -2;
    }
    bsp_tim1_pwm_deinit();
    g_tim1_cfg.prescaler   = (uint16_t)argc[0];
    g_tim1_cfg.period      = (uint16_t)argc[1];
    g_tim1_cfg.level_logic = (uint8_t)argc[2];
    if (bsp_tim1_pwm_init(&g_tim1_cfg)) {
        len = snprintf(buff, 25, "+TIM1_CFG:ERROR\r\n");
        pack->reply((uint8_t*)buff, len);
    } else {
        len = snprintf(buff, 25, "+TIM1_CFG:OK\r\n");
        pack->reply((uint8_t*)buff, len);
    }
    return 0;
}
ATCMD_INIT("AT+TIM1_CFG=", atcmd_set_tim1_cfg);
ATCMD_INIT("AT+TIM1_INFO?", atcmd_get_tim1_info);
#endif

#if LS_TIM2_EN
extern bsp_tim_cfg_t g_tim2_cfg;

static int atcmd_get_tim2_info(atcmd_pack_t *pack) {
    char buff[70] = {0};
    uint8_t len = 0;
    len = snprintf(buff, 70, "+TIM2_INFO:%u,%u,%u,%u,%u,%u,%u\r\n",
           g_tim2_cfg.prescaler, g_tim2_cfg.period, g_tim2_cfg.level_logic,
           bsp_pwm_get_pulse(4), bsp_pwm_get_pulse(5), bsp_pwm_get_pulse(6), bsp_pwm_get_pulse(7));
    pack->reply((uint8_t *)buff, len);
    return 0;
}

static int atcmd_set_tim2_cfg(atcmd_pack_t *pack) {
    uint32_t argc[3] = {0};
    char buff[25] = {0};
    uint8_t len = 0;
    pack->argc = sscanf((char*)(pack->data), "%u,%u,%u", &argc[0], &argc[1], &argc[2]);
    if (pack->argc != 3) {
        len = snprintf(buff, 25, "+TIM2_CFG:ARGCERR\r\n");
        pack->reply((uint8_t*)buff, len);
        return -2;
    }
    bsp_tim2_pwm_deinit();
    g_tim2_cfg.prescaler   = (uint16_t)argc[0];
    g_tim2_cfg.period      = (uint16_t)argc[1];
    g_tim2_cfg.level_logic = (uint8_t)argc[2];
    if (bsp_tim2_pwm_init(&g_tim2_cfg)) {
        len = snprintf(buff, 25, "+TIM2_CFG:ERROR\r\n");
        pack->reply((uint8_t*)buff, len);
    } else {
        len = snprintf(buff, 25, "+TIM2_CFG:OK\r\n");
        pack->reply((uint8_t*)buff, len);
    }
    return 0;
}
ATCMD_INIT("AT+TIM2_CFG=", atcmd_set_tim2_cfg);
ATCMD_INIT("AT+TIM2_INFO?", atcmd_get_tim2_info);
#endif

static int atcmd_get_pwm_val(atcmd_pack_t *pack) {
    char buff[70] = {0};
    uint8_t len = 0;
    len = snprintf(buff, 70, "+PWM:%u,%u,%u,%u,%u,%u,%u,%u\r\n",
    bsp_pwm_get_pulse(0), bsp_pwm_get_pulse(1), bsp_pwm_get_pulse(2), bsp_pwm_get_pulse(3),
    bsp_pwm_get_pulse(4), bsp_pwm_get_pulse(5), bsp_pwm_get_pulse(6), bsp_pwm_get_pulse(7));
    pack->reply((uint8_t *)buff, len);
    return 0;
}

static int atcmd_set_pwm_val(atcmd_pack_t *pack) {
    uint32_t argc[2] = {0};
    char buff[25] = {0};
    uint8_t len = 0;
    pack->argc = sscanf((char*)(pack->data), "%u,%u", &argc[0], &argc[1]);
    if (pack->argc != 2) {
        len = snprintf(buff, 25, "+PWM_CH:ARGCERR\r\n");
        pack->reply((uint8_t*)buff, len);
        return -2;
    }
    if (bsp_pwm_set_pulse(argc[0], argc[1])) {
        len = snprintf(buff, 25, "+PWM_CH:ERROR\r\n");
        pack->reply((uint8_t*)buff, len);
    } else {
        len = snprintf(buff, 25, "+PWM_CH:OK\r\n");
        pack->reply((uint8_t*)buff, len);
    }
    return 0;
}

ATCMD_INIT("AT+PWM_CH=", atcmd_set_pwm_val);
ATCMD_INIT("AT+PWM?", atcmd_get_pwm_val);
#endif
#endif
