/********************************************************************************
* @file    ls_key.h
* @author  jianqiang.xue
* @version V1.1.0
* @date    2023-07-14
* @brief   单键扫描/矩阵扫描
********************************************************************************/
#ifndef _LS_KEY_H
#define _LS_KEY_H

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>

/* Public Define -------------------------------------------------------------*/
/* Public Enum ---------------------------------------------------------------*/
typedef enum {
    KEY_EVENT_NULL = 0,
    KEY_EVENT_RELEASE,
    KEY_EVENT_PRESS,
    KEY_EVENT_TWO_PRESS,
    KEY_EVENT_SINGLE_CLICK,
    KEY_EVENT_DOUBLE_CLICK,
    KEY_EVENT_LONG_PRESS,
    KEY_EVENT_LONG_LONG_PRESS,
} key_event_t;
/* Public Struct -------------------------------------------------------------*/
typedef void(*ls_key_cb)(key_event_t event);

/* Public Define -------------------------------------------------------------*/
/* External Variables --------------------------------------------------------*/
/* Public Macro --------------------------------------------------------------*/
/* Public Function Prototypes ------------------------------------------------*/
// 引脚默认类型为 KEY 类型的引脚才能生效，如果是output,iic,spi,uart等引脚则忽略本次注册！sysgui可以查询。
uint8_t ls_key_reg(uint8_t io, ls_key_cb cb); // 将指定io注册到按键扫描

uint8_t ls_key_init(void *event_cb); // 创建软定时器扫描IO
void ls_key_deinit(void);

void ls_key_start_scan(void);
void ls_key_stop_scan(void);
#endif
