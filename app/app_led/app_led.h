/********************************************************************************
* @file    app_led.h
* @author  jianqiang.xue
* @version V1.0.0
* @date    2021-04-20
* @brief   LED灯光效果
********************************************************************************/

#ifndef __APP_LED_H
#define __APP_LED_H

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include "ls_syscfg.h"

/* Publib Enum ---------------------------------------------------------------*/
typedef enum
{
    LED_TYPE_OFF = 0,
    LED_TYPE_LIGHT,
    LED_TYPE_BREATH,
    LED_TYPE_BREATH_LAMP,
    LED_TYPE_TWINKLE,
    LED_TYPE_SOS,
    LED_TYPE_RISE_SLOWLY,
    LED_TYPE_FALL_SLOWLY,

    LED_TYPE_RISE_SLOWLY_TWINKLE,
    LED_TYPE_RISE_SLOWLY_TWINKLE_LIGHT_OFF_LOOP,

    LED_TYPE_BREATH_TWINKLE,
    LED_TYPE_RISE_SLOWLY_TWINKLE_RISE_FALL_OFF_LOOP,

    LED_TYPE_OFF_RISE_SLOWLY_FALL_LIGHT,
    LED_TYPE_BREATH_TOP_LOW,
    LED_TYPE_RISE_SLOWLY_FALL_SLOWLY_OFF_LOOP,
#if LS_APP_LED_WS2812_DRIVEN_MODE
    LED_TYPE_RED_BLUE_TWINKLE,
    LED_TYPE_IRIDESCENCE,
#endif
    LED_TYPE_MAX,
} app_led_type_t;

typedef enum
{
    APP_LED_ID_0 = 0,
    APP_LED_ID_1,
    APP_LED_ID_2,
    APP_LED_ID_3,
    APP_LED_ID_4,
    APP_LED_ID_5,
    APP_LED_ID_6,
    APP_LED_ID_7,
    APP_LED_ID_8,
    APP_LED_ID_9,
    APP_LED_ID_10,
    APP_LED_ID_11
} app_led_id_t;

typedef enum
{
    LED_DRIVEN_GPIO = 0,
    LED_DRIVEN_PWM,
    LED_DRIVEN_WS2812
} led_driven_t;

/* Public Function Prototypes -----------------------------------------------*/
bool app_led_init(void);
bool app_led_indicate(led_driven_t led_driven, app_led_id_t led_id, app_led_type_t type, uint16_t cycle_time,
                      uint32_t period, uint16_t crest_time, uint16_t trough_time);

uint16_t app_led_pwm_get_current_period(app_led_id_t led_id);
app_led_type_t app_led_pwm_get_current_type(app_led_id_t led_id);

bool app_task_led(void);
#endif
