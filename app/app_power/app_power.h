/********************************************************************************
* @file    app_power.h
* @author  jianqiang.xue
* @version V1.0.0
* @date    2021-06-09
* @brief   电源管理
********************************************************************************/

#ifndef __APP_POWER_H
#define __APP_POWER_H

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>

/* Publib Enum ---------------------------------------------------------------*/
typedef enum
{
    POWER_SHUTDOWN_BATTERY = 0,
    POWER_VERY_LOW_BATTERY,
    POWER_LOW_LOW_BATTERY,
    POWER_LOW_BATTERY,
    POWER_MIDDLE_BATTERY,
    POWER_HIGH_BATTERY,
    POWER_FULL_BATTERY,
}power_bat_state_t;

typedef enum
{
    POWER_NOT_CHARGE = 0,
    POWER_CHARGING,
    POWER_CHARGE_DONE
}power_charge_state_t;

/* Publib Struct ------------------------------------------------------------*/
typedef struct
{
    bool write_in;                        // 0--false 1--true
    power_bat_state_t bat_state;          // 根据电压得到电池状态
    short bat_vol_offset;                 // 电池电压值 偏差值 offset (减去记录充满时的电压值)
    uint8_t  bat_sos;                     // 电池电压百分比值
    uint16_t bat_vol;                     // 电池电压值
    uint16_t charge_full_vol_val;     // 记录充满时的电压值
}bat_info_t;

typedef struct
{
    bool usb_state;                       // 0--未插入  1--插入
    bool charge_state;                    // 0--未充满  1--充满
    bat_info_t bat_info;
} power_info_t;

/* Public Function Prototypes -----------------------------------------------*/

/***********状态查询设置***********/
void* get_power_info_p(void);
void* get_bat_info_p(void);

void set_usb_state(bool state);
bool get_usb_state(void);

bool get_charge_state(void);
void set_charge_state(bool state);

void set_bat_soc_val(uint8_t soc);
void increment_bat_soc_val(uint8_t soc);
void reduce_bat_soc_val(uint8_t soc);
uint8_t get_bat_soc_val(void);

void set_bat_vol_offset(short offset);
short get_bat_vol_offset(void);

power_bat_state_t get_bat_state(void);
void set_bat_state(power_bat_state_t state);

uint16_t get_bat_full_vol_val(void);
void set_bat_full_vol_val(uint16_t vol);

uint16_t get_bat_vol(void);
void set_bat_vol(uint16_t vol);
void reduce_bat_vol(uint16_t vol);

bool get_bat_whether_write(void);
void set_bat_whether_write(bool state);

// 得到当前电池状态
power_bat_state_t app_power_get_bat_state(uint16_t bat_soc, uint8_t usb_state);

// [通过充电IC单个引脚逻辑判断 或者直接引脚判断]
power_charge_state_t app_power_get_charge_state(void);
bool app_power_get_usb_state(void);

// [通过充电IC的两个引脚逻辑判断]
power_charge_state_t app_power_get_charge_state2(void);
bool app_power_get_usb_state2(void);
#endif
