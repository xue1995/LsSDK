/********************************************************************************
* @file    app_soft_voltameter.h
* @author  jianqiang.xue
* @version V1.0.0
* @date    2021-07-14
* @brief   软件电量计
********************************************************************************/

#ifndef __APP_SOFT_VOLTAMETER_H
#define __APP_SOFT_VOLTAMETER_H

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>

/* Public Function Prototypes ------------------------------------------------*/

void power_bat_state_handle(bool first, uint16_t bat_voltage);
uint16_t get_bat_full_vol_model_val(void);
#endif
