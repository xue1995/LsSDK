/********************************************************************************
* @file    app_temp_control.c
* @author  jianqiang.xue
* @version V1.0.0
* @date    2021-06-16
* @brief   温度管理
********************************************************************************/

/* Private includes ----------------------------------------------------------*/
#include <stdio.h>
#include <stdint.h>

#include "ls_syscfg.h"

/* Private define ------------------------------------------------------------*/
#define carry_round(num)      ((num * 10) + 5) / 10
#define carry_percent(VOL)    (((VOL - NORMAL_TEMP_VAL) * 1.0) / ((MAX_TEMP_VAL - NORMAL_TEMP_VAL) / 100.0))

/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/**
  * @brief  The temperature control coefficient of the main lamp is obtained
  * @param  temp 温度值[-25000,125000]
  * @return [0-100]
  */
uint8_t get_lamp_temp_coefficient(int16_t temp)
{
    // 温度到极限了
    if (temp >= MAX_TEMP_VAL)
    {
        return 5;
    }
    // 热敏电阻开路
    else if (temp <= OPEN_CIRCUIT_TEMP_VAL)
    {
        return 5;
    }
    // 恢复正常
    else if (temp <= NORMAL_TEMP_VAL)
    {
        return 100;
    }
    // 触发温控值
    else if (temp > NORMAL_TEMP_VAL)
    {
        uint8_t val = 0;
        val = 100.0 - carry_round(carry_percent(temp));
        if (val <= 20)
        {
            val = 20;
        }
        else if (val >= 100)
        {
            val = 100;
        }
        return (uint8_t)val;
    }
    return 5;
}
