/********************************************************************************
* @file    app_temp_control.h
* @author  jianqiang.xue
* @version V1.0.0
* @date    2021-06-16
* @brief   温度管理
********************************************************************************/

#ifndef __APP_TEMP_CONTROL_H
#define __APP_TEMP_CONTROL_H

#include <stdint.h>

uint8_t get_lamp_temp_coefficient(int16_t temp);

#endif
