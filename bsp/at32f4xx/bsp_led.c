/********************************************************************************
* @file    bsp_led.c
* @author  jianqiang.xue
* @version V1.0.0
* @date    2021-10-10
* @brief   LED控制
********************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "RTE_Components.h"
#include CMSIS_device_header

#include "bsp_gpio.h"
#include "bsp_led.h"
/* Private Includes ----------------------------------------------------------*/
#include "ls_gpio.h"
#include "ls_syscfg.h"

/* Private Define ------------------------------------------------------------*/
/* Private Variables ---------------------------------------------------------*/
#if BS_LEDN != 0
static const bsp_gpio_t g_gpio_init[BS_LEDN] =
{
#if (BS_LEDN > 0)
    {BS_LED0_GPIO_PORT, BS_LED0_PIN, BS_LED0_GPIO_CLK, BS_LED0_LIGHT_LEVEL, 0, 0, 0, 0, 0},
#endif
#if (BS_LEDN > 1)
    {BS_LED1_GPIO_PORT, BS_LED1_PIN, BS_LED1_GPIO_CLK, BS_LED1_LIGHT_LEVEL, 0, 0, 0, 0, 0},
#endif
#if (BS_LEDN > 2)
    {BS_LED2_GPIO_PORT, BS_LED2_PIN, BS_LED2_GPIO_CLK, BS_LED2_LIGHT_LEVEL, 0, 0, 0, 0, 0},
#endif
#if (BS_LEDN > 3)
    {BS_LED3_GPIO_PORT, BS_LED3_PIN, BS_LED3_GPIO_CLK, BS_LED3_LIGHT_LEVEL, 0, 0, 0, 0, 0},
#endif
#if (BS_LEDN > 4)
    {BS_LED4_GPIO_PORT, BS_LED4_PIN, BS_LED4_GPIO_CLK, BS_LED4_LIGHT_LEVEL, 0, 0, 0, 0, 0},
#endif
#if (BS_LEDN > 5)
    {BS_LED5_GPIO_PORT, BS_LED5_PIN, BS_LED5_GPIO_CLK, BS_LED5_LIGHT_LEVEL, 0, 0, 0, 0, 0},
#endif
#if (BS_LEDN > 6)
    {BS_LED6_GPIO_PORT, BS_LED6_PIN, BS_LED6_GPIO_CLK, BS_LED6_LIGHT_LEVEL, 0, 0, 0, 0, 0},
#endif
#if (BS_LEDN > 7)
    {BS_LED7_GPIO_PORT, BS_LED7_PIN, BS_LED7_GPIO_CLK, BS_LED7_LIGHT_LEVEL, 0, 0, 0, 0, 0},
#endif
};

/* Public Function Prototypes -----------------------------------------------*/
/**
 * @brief  led初始化 使能引脚时钟 配置为推免输出
 */
void bsp_led_init(void)
{
    uint8_t i;

    for (i = 0; i < BS_LEDN; i++)
    {
        /* Enable the GPIO_LED Clock */
        bsp_gpio_set_clk(GPIO_APBx, g_gpio_init[i].periph, true);

        /* Configure the GPIO_LED pin */
        bsp_gpio_init_output(g_gpio_init[i].port, g_gpio_init[i].pin, BSP_GPIO_PIN_OUT_PP);

        /* Reset PIN to switch off the LED */
        bsp_gpio_set_pin(g_gpio_init[i].port, g_gpio_init[i].pin, (bsp_gpio_pin_state_t)!g_gpio_init[i].level);
    }
}

/**
 * @brief  led反初始化 将引脚配置为浮空输入
 */
void bsp_led_deinit(void)
{
    uint8_t i;

    if (BS_LEDN == 0)
    {
        return;
    }

    for (i = 0; i < BS_LEDN; i++)
    {
        /* Turn off LED */
        bsp_gpio_set_pin(g_gpio_init[i].port, g_gpio_init[i].pin, (bsp_gpio_pin_state_t)!g_gpio_init[i].level);
        /* DeInit the GPIO_LED pin */
        bsp_gpio_deinit(g_gpio_init[i].port, g_gpio_init[i].pin);
    }
}

/**
 * @brief  点亮LED
 * @param  ch: LED组号
 */
void bsp_led_on(bsp_led_t ch)
{
    if (ch >= BS_LEDN)
    {
        return;
    }
    bsp_gpio_set_pin(g_gpio_init[ch].port, g_gpio_init[ch].pin, (bsp_gpio_pin_state_t)g_gpio_init[ch].level);
}

/**
 * @brief  关闭LED
 * @param  ch: LED组号
 */
void bsp_led_off(bsp_led_t ch)
{
    if (ch >= BS_LEDN)
    {
        return;
    }
    bsp_gpio_set_pin(g_gpio_init[ch].port, g_gpio_init[ch].pin, (bsp_gpio_pin_state_t)!g_gpio_init[ch].level);
}

/**
 * @brief  翻转LED
 * @param  ch: LED组号
 */
void bsp_led_toggle(bsp_led_t ch)
{
    if (ch >= BS_LEDN)
    {
        return;
    }
    bsp_gpio_set_toggle(g_gpio_init[ch].port, g_gpio_init[ch].pin);
}
#else
void bsp_led_init(void)
{
}
void bsp_led_deinit(void)
{
}
void bsp_led_on(bsp_led_t ch)
{
}
void bsp_led_off(bsp_led_t ch)
{
}
void bsp_led_toggle(bsp_led_t ch)
{
}
#endif
