/********************************************************************************
* @file    bsp_tim.c
* @author  jianqiang.xue
* @version V1.0.0
* @date    2022-02-14
* @brief   NULL
********************************************************************************/
/* Includes ----------------------------------------------------------*/
#include <stdio.h>
#include <string.h>

#include "RTE_Components.h"
#include CMSIS_device_header

#include "bsp_gpio.h"
#include "bsp_tim.h"

/* Private Includes ----------------------------------------------------------*/
#include "ls_gpio.h"
#include "ls_syscfg.h"

/* Private Define ------------------------------------------------------------*/
/* Private Typedef -----------------------------------------------------------*/
#if BS_TIM1_EN
TMR_TimerBaseInitType tim1_handle_t =
{
    .TMR_Period            = BS_TIM1_PERIOD,                     // 重装载值
    .TMR_DIV               = BS_TIM1_PRESCALER,                  // 预分频
    .TMR_ClockDivision     = 0,                                  // CKD 时钟分频因子(Clock division)
    .TMR_CounterMode       = TMR_CounterDIR_Up,                  // 边沿对齐模式 计数器向上计数
    .TMR_RepetitionCounter = 0,                                  // TIM1_RCR 重复计数器的值
};

TMR_OCInitType tim1_oc_init_handle_t =
{
    .TMR_OCMode         = TMR_OCMode_PWM1,        // PWM模式1 TIM1_CCMR1
    .TMR_Pulse          = BS_TIM1_PERIOD,         // CCR 捕获/比较通道
    .TMR_OutputState    = TMR_OutputState_Enable, // 输出模式
    .TMR_OCPolarity     = TMR_OCPolarity_High     // 高电平有效
};
#endif


void bsp_tim_pwm_init(void)
{
#if BS_TIM1_EN
    /* TMR1 clocks enable */
    RCC_APB2PeriphClockCmd(RCC_APB2PERIPH_TMR1, ENABLE);
    /* Set TIMx instance */
    TMR_TimeBaseInit(TMR1, &tim1_handle_t);

#if BS_PWM0_EN
    /* Enable the Clock */
    bsp_gpio_set_clk(GPIO_APBx, BS_TIM1_CH1_GPIO_CLK, true);
    bsp_gpio_init_tim(BS_TIM1_CH1_GPIO_PORT, BS_TIM1_CH1_PIN, 0);
    TMR_OC1Init(TMR1, &tim1_oc_init_handle_t);
    TMR_OC1PreloadConfig(TMR1, TMR_OCPreload_Enable);  //CH1预装载使能
#endif

#if BS_PWM1_EN
    /* Enable the Clock */
    bsp_gpio_set_clk(GPIO_APBx, BS_TIM1_CH2_GPIO_CLK, true);
    bsp_gpio_init_tim(BS_TIM1_CH2_GPIO_PORT, BS_TIM1_CH2_PIN, 0);
    TMR_OC2Init(TMR1, &tim1_oc_init_handle_t);
    TMR_OC2PreloadConfig(TMR1, TMR_OCPreload_Enable);  //CH2预装载使能
#endif

#if BS_PWM2_EN
    /* Enable the Clock */
    bsp_gpio_set_clk(GPIO_APBx, BS_TIM1_CH3_GPIO_CLK, true);
    bsp_gpio_init_tim(BS_TIM1_CH3_GPIO_PORT, BS_TIM1_CH3_PIN, 0);
    TMR_OC3Init(TMR1, &tim1_oc_init_handle_t);
    TMR_OC3PreloadConfig(TMR1, TMR_OCPreload_Enable);  //CH3预装载使能
#endif

#if BS_PWM3_EN
    /* Enable the Clock */
    bsp_gpio_set_clk(GPIO_APBx, BS_TIM1_CH4_GPIO_CLK, true);
    bsp_gpio_init_tim(BS_TIM1_CH4_GPIO_PORT, BS_TIM1_CH4_PIN, 0);
    TMR_OC4Init(TMR1, &tim1_oc_init_handle_t);
    TMR_OC4PreloadConfig(TMR1, TMR_OCPreload_Enable);  //CH4预装载使能
#endif

    /* TMR1 Main Output Enable
    普通定时器在完成以上设置了之后， 就可以输出 PWM 了，
    但是高级定时器，我们还需要使能刹车和死区寄存器（ TIM1_BDTR）的 MOE 位，以使能整个 OCx（即 PWM）输出。
    */
    TMR_CtrlPWMOutputs(TMR1, ENABLE);
    //使能TIMx在ARR上的预装载寄存器
    TMR_ARPreloadConfig(TMR1, ENABLE);
    /* TMR1 counter enable */
    TMR_Cmd(TMR1, ENABLE);
#endif
}

void bsp_tim_pwm_deinit(void)
{
#if BS_TIM1_EN
#if BS_PWM0_EN
    bsp_gpio_deinit(BS_TIM1_CH1_GPIO_PORT, BS_TIM1_CH1_PIN);
#endif
#if BS_PWM1_EN
    bsp_gpio_deinit(BS_TIM1_CH2_GPIO_PORT, BS_TIM1_CH2_PIN);
#endif
#if BS_PWM2_EN
    bsp_gpio_deinit(BS_TIM1_CH3_GPIO_PORT, BS_TIM1_CH3_PIN);
#endif
#if BS_PWM3_EN
    bsp_gpio_deinit(BS_TIM1_CH4_GPIO_PORT, BS_TIM1_CH4_PIN);
#endif
    TMR_Reset(TMR1);
    RCC_APB2PeriphClockCmd(RCC_APB2PERIPH_TMR1, DISABLE);
#endif
}

void bsp_tim_set_period(bsp_tim_t tim_id, uint16_t period)
{
    if (tim_id == BSP_TIM_0)
    {
    }
    else if (tim_id == BSP_TIM_1)
    {
        TMR_SetAutoreload(TMR1, period);
    }
}
