/********************************************************************************
* @file    bsp_adc.h
* @author  jianqiang.xue
* @version V1.1.0
* @date    2023-03-06
* @brief   ADC操作
********************************************************************************/

#ifndef __BSP_ADC_H
#define __BSP_ADC_H

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>

typedef struct {
    uint8_t ch_val;     // 通道值： 例如:使能ch5,ch6，则 ch_val |= (1<<5)|(1<<6);
    uint8_t clk_sel;    // ADC时钟分频 0-不分频, 1-PCLK/2, 2-PCLK/4, 3-PCLK/8, 4-PCLK/16, ..., 7-PCLK/128
    uint8_t sample_num; // 采用转换次数，即每次启动采样，需要num次才完成。
} bsp_adc_cfg_t;

#if LS_ADC0_EN
extern bsp_adc_cfg_t g_adc0_cfg;
#endif
/* Public Function Prototypes -----------------------------------------------*/
void io_cfg_as_adc0(uint8_t io);

uint8_t bsp_adc0_init(void);
void bsp_adc0_deinit(void);

void bsp_adc0_start(void);
void bsp_adc0_stop(void);

uint16_t bsp_adc0_get_ch_val(uint8_t ch_num);

uint8_t bsp_adc0_irq_callback(void *event);

#endif
