/********************************************************************************
* @file    bsp_crc.h
* @author  jianqiang.xue
* @version V1.0.0
* @date    2021-04-15
* @brief
********************************************************************************/

#ifndef __BSP_CRC_H
#define __BSP_CRC_H

#include <stdint.h>

void bsp_crc_init(void);
uint32_t bsp_crc_calculate(uint8_t *data, uint32_t len);

#endif
