/********************************************************************************
* @file    bsp_exti.h
* @author  jianqiang.xue
* @version V1.1.0
* @date    2023-06-04
* @brief   设置中断优先级和注册中断回调函数
********************************************************************************/

#ifndef __BSP_EXTI_H
#define __BSP_EXTI_H

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>

/* Public Function Prototypes ------------------------------------------------*/

void bsp_exti_set(uint8_t irqn, uint32_t priority);
void bsp_exti_clear_set(uint8_t irqn);
void bsp_exti_clear_flag(uint8_t io);

// 引脚外部中断回调

bool bsp_gpio_exti_irq_reg_cb(void *event);

#endif
