/********************************************************************************
* @file    bsp_flash.h
* @author  jianqiang.xue
* @version V1.0.0
* @date    2021-04-03
* @brief   flash操作
********************************************************************************/

#ifndef __BSP_FLASH_H
#define __BSP_FLASH_H

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>

/* Public struct ------------------------------------------------------------*/

typedef struct
{
    uint8_t  boot_state;      // 0--run app  1--in dfu  2--move ota in app
    uint8_t  boot_run_cnt;    // 进入boot的次数 当进入boot次数累计 10< cnt < 20,停留在boot, >20程序自毁
    uint16_t run_app_cnt;     // APP首次运行3秒以上，自增。 -- 总运行次数
    uint32_t boot_carry_size; // 需要复制的字节数量
    uint16_t app_file_sum;    // app 运行前校验累计和，防止app损坏，当sum为0x00或0xFFFF时，不校验
    uint16_t chip_lock;       // 任意值--锁住swo XXXX--特殊码解锁swo
} boot_info_t;

/* Public Function Prototypes ------------------------------------------------*/

uint8_t bsp_flash_erase_page(uint32_t page_addr, uint32_t page_num);

uint8_t bsp_flash_read_byte(uint32_t addr);
bool bsp_flash_read_nbyte(uint32_t addr, uint8_t *data, uint32_t len);

uint16_t bsp_flash_read_halfword(uint32_t addr);
uint32_t bsp_flash_read_word(uint32_t addr);

bool bsp_flash_write_byte(uint32_t addr, uint8_t data);
bool bsp_flash_write_nbyte(uint32_t addr, uint8_t *data, uint32_t len);
bool bsp_flash_write_nbyte_s(uint32_t addr, uint8_t *data, uint32_t len);
bool bsp_flash_write_word(uint32_t addr, uint32_t data);

bool bsp_flash_is_busy(void);
bool bsp_flash_carry(uint32_t t_addr, uint32_t s_addr, uint32_t size);

#endif
