/********************************************************************************
* @file    bsp_i2c.h
* @author  jianqiang.xue
* @version V1.0.0
* @date    2021-04-11
* @brief   NULL
********************************************************************************/

#ifndef __BSP_I2C_H
#define __BSP_I2C_H

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>

/* Public enum ---------------------------------------------------------------*/
typedef enum {
    I2C_BUS0 = 0,
    I2C_BUS1 = 1,
    I2C_BUS2 = 2,
} bsp_i2c_bus_t;

/* Public Function Prototypes ------------------------------------------------*/

uint8_t bsp_i2c_init(bsp_i2c_bus_t i2c_bus, uint8_t scl, uint8_t sda);
void bsp_i2c_deinit(bsp_i2c_bus_t i2c_bus);

// 成品全套逻辑 启动-应答-数据-停止

uint8_t bsp_i2c_read_byte(bsp_i2c_bus_t i2c_bus, uint8_t dev_addr, uint8_t reg_addr, uint8_t *r_data);
uint8_t bsp_i2c_read_nbyte(bsp_i2c_bus_t i2c_bus, uint8_t dev_addr, uint8_t *w_data, uint16_t w_size, uint8_t *r_data, uint16_t r_size);

uint8_t bsp_i2c_write_byte(bsp_i2c_bus_t i2c_bus, uint8_t dev_addr, uint8_t reg_addr, uint8_t w_data);
uint8_t bsp_i2c_write_nbyte(bsp_i2c_bus_t i2c_bus, uint8_t dev_addr, uint8_t *w_data, uint16_t w_size);

// 散装函数

uint8_t bsp_i2c_write_nbyte_nostop(bsp_i2c_bus_t i2c_bus, uint8_t dev_addr, uint8_t *w_data, uint16_t w_size);
uint8_t bsp_i2c_send_nbyte(bsp_i2c_bus_t i2c_bus, uint8_t *w_data, uint16_t w_size);
uint8_t bsp_i2c_stop(bsp_i2c_bus_t i2c_bus, bool state);
#endif
