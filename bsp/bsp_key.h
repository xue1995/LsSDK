/********************************************************************************
* @file    bsp_key.h
* @author  jianqiang.xue
* @version V1.0.0
* @date    2021-04-03
* @brief   bsp key
********************************************************************************/

#ifndef __BSP_KEY_H
#define __BSP_KEY_H

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>

/* Public enum ---------------------------------------------------------------*/
typedef enum {
    BSP_KEY_X_0 = 0,
    BSP_KEY_X_1,
    BSP_KEY_X_2,
    BSP_KEY_X_3,
    BSP_KEY_X_4,
    BSP_KEY_X_5,
    BSP_KEY_X_6,
    BSP_KEY_X_7,
    BSP_KEY_X_8,
    BSP_KEY_X_9,
    BSP_KEY_X_10,
    BSP_KEY_X_11,
    BSP_KEY_X_12,
    BSP_KEY_X_13,
    BSP_KEY_X_14,
    BSP_KEY_X_15
} bsp_key_t;

typedef enum {
    KEY_MODE_GPIO = 0,
    KEY_MODE_EXTI = 1
} bsp_key_mode_t;

/* Public Function Prototypes ------------------------------------------------*/

void bsp_key_init(void);
void bsp_key_deinit(bsp_key_t ch);

void bsp_key_set_irq(bsp_key_t ch);
void bsp_key_clear_irq(bsp_key_t ch);

bool bsp_key_get_state(bsp_key_t ch);

bool bsp_key_irq_callback(bsp_key_t ch, void *event);

#endif
