/********************************************************************************
* @file    bsp_led.h
* @author  jianqiang.xue
* @version V1.0.0
* @date    2021-04-03
* @brief   NULL
********************************************************************************/

#ifndef __BSP_LED_H
#define __BSP_LED_H

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>

/* Public enum ---------------------------------------------------------------*/
typedef enum {
    BSP_LED_0 = 0,
    BSP_LED_1,
    BSP_LED_2,
    BSP_LED_3,
    BSP_LED_4,
    BSP_LED_5,
    BSP_LED_6,
    BSP_LED_7
} bsp_led_t;

/* Public Function Prototypes ------------------------------------------------*/

void bsp_led_init(void);
void bsp_led_deinit(void);

void bsp_led_on(bsp_led_t ch);
void bsp_led_off(bsp_led_t ch);
void bsp_led_toggle(bsp_led_t ch);

#endif
