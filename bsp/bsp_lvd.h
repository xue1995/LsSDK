/********************************************************************************
* @file    bsp_lvd.h
* @author  jianqiang.xue
* @version V1.0.0
* @date    2021-12-18
* @brief   检测电源电压
********************************************************************************/

#ifndef __BSP_LVD_H
#define __BSP_LVD_H

#include <stdint.h>

void bsp_lvd_init(void);
void bsp_lvd_deinit(void);
uint16_t get_lvd_vol_val(void);
void bsp_lvd_start_check(void);
void bsp_lvd_process_check(void);
uint16_t get_lvd_check_vol_val(void);
#endif
