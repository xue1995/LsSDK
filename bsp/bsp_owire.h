/********************************************************************************
* @file    bsp_owire.h
* @author  jianqiang.xue
* @version V1.0.0
* @date    2021-11-22
* @brief   单总线协议(One-Wire)
********************************************************************************/
/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

/* Public Function Prototypes ------------------------------------------------*/

bool bsp_onewire_send_data(uint8_t *data, uint16_t len);
void bsp_onewire_init(void);
