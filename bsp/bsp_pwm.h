/********************************************************************************
 * @file    bsp_pwm.h
 * @author  jianqiang.xue
 * @version V2.0.0
 * @date    2023-07-11
 * @brief   改变写法，IO输入
 ********************************************************************************/

#ifndef __BSP_PWM_H
#define __BSP_PWM_H

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>

/* Public enum ---------------------------------------------------------------*/
typedef enum {
    BSP_PWM_0 = 0,
    BSP_PWM_1,
    BSP_PWM_2,
    BSP_PWM_3,
    BSP_PWM_4,
    BSP_PWM_5,
    BSP_PWM_6,
    BSP_PWM_7,
    BSP_PWM_8,
    BSP_PWM_9,
    BSP_PWM_10,
    BSP_PWM_11,
    BSP_PWM_MAX
} bsp_pwm_t;

/* Public Function Prototypes ------------------------------------------------*/

uint8_t bsp_pwm_set_pulse(uint8_t io, uint16_t val);
uint16_t bsp_pwm_get_pulse(uint8_t io);
uint16_t bsp_pwm_pp_to_pulse(uint8_t io, uint8_t pp);
uint16_t bsp_pwm_get_max_period(uint8_t io);
#endif
