/********************************************************************************
* @file    bsp_rng.h
* @author  jianqiang.xue
* @version V1.0.0
* @date    2021-07-09
* @brief   随机数生成
********************************************************************************/

#ifndef __BSP_RNG_H
#define __BSP_RNG_H

#include <stdint.h>

void random_vector_generate(uint8_t * pbuff, uint8_t size);
#endif
