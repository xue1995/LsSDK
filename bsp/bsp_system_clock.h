/********************************************************************************
* @file    bsp_system_clock.h
* @author  jianqiang.xue
* @version V1.0.0
* @date    2021-04-03
* @brief
********************************************************************************/

#ifndef __BSP_SYSTEM_CLOCK_H
#define __BSP_SYSTEM_CLOCK_H

#include <stdint.h>

void bsp_system_clock_config(void);

#endif
