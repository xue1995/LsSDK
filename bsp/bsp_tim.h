/********************************************************************************
* @file    bsp_tim.c
* @author  jianqiang.xue
* @version V1.2.0
* @date    2023-06-04
* @brief   本文件不再初始化GPIO，由APP_IO完成引脚初始化和复用。
********************************************************************************/

#ifndef __BSP_TIM_H
#define __BSP_TIM_H

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include "stdbool.h"
/* Public Define -------------------------------------------------------------*/
/* Public Enum ---------------------------------------------------------------*/
typedef enum {
    BSP_TIM_0 = 0,
    BSP_TIM_1
} bsp_tim_t;

typedef enum {
    TIM_FUNC_CNT = 0,
    TIM_FUNC_PWM
} bsp_tim_func_t;

typedef enum {
    TIM_MODE_ONE = 0,
    TIM_MODE_PERIODIC
} bsp_tim_mode_t;
/* Public Typedef ------------------------------------------------------------*/
typedef struct { // pwm hz = sys_clock / (Prescaler+1) / (Period+1)
    uint16_t prescaler;  // 预分频器的值(Prescaler value) 计数器的时钟频率(CK_CNT)等于 fCK_PSC/(PSC[15:0]+1)。
    uint16_t period;     // 周期  计数器的值(Counter value)--CNT
    uint8_t level_logic; // 有效电平   0--低电平  1--高电平
} bsp_tim_cfg_t;

typedef void (*bsp_tim_cb)(void);
typedef struct {
    bsp_tim_func_t type : 4;  // 0--计数器  1--pwm
    uint8_t priority : 3;     // 中断优先级 0-3
    bsp_tim_mode_t mode : 1;  // 0--单次  1--循环(PWM模式请设置循环)

    uint16_t prescaler;   // 预分频器的值(Prescaler value) 计数器的时钟频率(CK_CNT)等于 fCK_PSC/(PSC[15:0]+1)。
    uint16_t freq;        // 单位f,1s内变化的次数
    union func_t {
        struct { // 【计数器模式】计数器模式，1000f, 1s/1000=1000ms/1000=1ms,即每1ms触发一次中断回调。
            bsp_tim_cb cb;     // 时间到就执行回调函数
        } tim;

        struct { // 【PWM模式】作为PWM时，period对应的freq：20f--50ms  40f--25ms  200--5ms
            uint8_t level_logic; // 有效电平   0--低电平  1--高电平
            uint8_t io;   // 引脚号，由sysgui工具查看
            uint16_t cnt; // 计数器的值(Counter value)--CNT,用来调整占空比.
        } pwm; // PWM周期 = period * cnt. 比如freq=200f,tim周期5ms,触发一次回调，cnt=100,则pwm周期=5*100=500ms. 即pwm频率为1/0.5s=2000Hz.
    } func;
} bsp_base_tim_cfg_t; // 基础定时器配置功能
/* Public Function Prototypes ------------------------------------------------*/

uint8_t bsp_tim1_pwm_init(bsp_tim_cfg_t *cfg);
void bsp_tim1_pwm_deinit(void);
void bsp_tim1_pwm_ch_en(uint8_t ch, uint8_t en);

void bsp_tim1_drive_pwm_init(uint8_t io_ch1, uint8_t io_ch2,
                             uint8_t io_ch3, uint8_t io_ch4);



uint8_t bsp_tim2_pwm_init(bsp_tim_cfg_t *cfg);
void bsp_tim2_pwm_deinit(void);
void bsp_tim2_pwm_ch_en(uint8_t ch, uint8_t en);

void bsp_tim2_drive_pwm_init(uint8_t io_ch1, uint8_t io_ch2,
                             uint8_t io_ch3, uint8_t io_ch4);



uint8_t bsp_tim10_init(bsp_base_tim_cfg_t *cfg);
void bsp_tim10_set_pwm(uint8_t io, uint8_t val);
void bsp_tim10_start(void);
void bsp_tim10_stop(void);



uint8_t bsp_tim11_init(bsp_base_tim_cfg_t *cfg);
void bsp_tim11_set_pwm(uint8_t io, uint8_t val);
void bsp_tim11_start(void);
void bsp_tim11_stop(void);


#endif
