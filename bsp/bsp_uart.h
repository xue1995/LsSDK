/********************************************************************************
* @file    bsp_uart.h
* @author  jianqiang.xue
* @version V1.0.0
* @date    2021-04-13
* @brief   NULL
********************************************************************************/

#ifndef __BSP_UART_H
#define __BSP_UART_H

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>

/* Public enum ---------------------------------------------------------------*/
typedef enum {
    BSP_UART_0 = 0,
    BSP_UART_1 = 1,
    BSP_UART_2 = 2,
} bsp_uart_t;

/* Public Function Prototypes ------------------------------------------------*/

// uart基础功能

uint8_t bsp_uart_init(bsp_uart_t uart, uint32_t baud, uint8_t tx_pin, uint8_t rx_pin);
void bsp_uart_deinit(bsp_uart_t uart);
uint8_t bsp_uart_set_baud_rate(bsp_uart_t uart, uint32_t baud);

void bsp_uart_rx_close(bsp_uart_t uart);
void bsp_uart_rx_open(bsp_uart_t uart);

bool bsp_uart_rx_irq_callback(bsp_uart_t uart, void *event);

// uart发送函数

uint8_t bsp_uart_send_byte(bsp_uart_t uart, uint8_t data);
uint8_t bsp_uart_send_nbyte(bsp_uart_t uart, uint8_t* data, uint16_t len);
void bsp_uart_send_nbyte_nowait(bsp_uart_t uart, uint8_t *data, uint16_t len);

bool bsp_uart_get_bus(bsp_uart_t uart);

// 获得rx缓冲区首指针

uint8_t *bsp_uart_get_rxbuff(bsp_uart_t uart);

uint16_t bsp_uart_get_rxbuff_position(bsp_uart_t uart);
void bsp_uart_set_rxbuff_position(bsp_uart_t uart, uint16_t val);
void bsp_uart_add_rxbuff_position(bsp_uart_t uart);

uint16_t bsp_uart_get_rxbuff_size(bsp_uart_t uart);
void bsp_uart_reset_rxbuff(bsp_uart_t uart);

#endif
