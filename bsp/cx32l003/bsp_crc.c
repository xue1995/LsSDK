/********************************************************************************
* @file    bsp_crc.c
* @author  jianqiang.xue
* @version V1.0.0
* @date    2021-04-15
* @brief   CRC校验
********************************************************************************/

#include "RTE_Components.h"
#include CMSIS_device_header
#include "cx32l003_hal.h"


static CRC_HandleTypeDef g_crc_config =
{
    .Instance = CRC
};

void bsp_crc_init(void)
{
    __HAL_RCC_CRC_CLK_ENABLE();
    HAL_CRC_Init(&g_crc_config);
}

uint32_t bsp_crc_calculate(uint8_t *data, uint32_t len)
{
    return HAL_CRC_Calculate(&g_crc_config, data, len);
}
