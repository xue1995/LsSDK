/********************************************************************************
 * @file    bsp_exti.c
 * @author  jianqiang.xue
 * @version V1.0.0
 * @date    2021-04-09
 * @brief   NULL
 ********************************************************************************/
/* Includes ------------------------------------------------------------------*/
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "cx32l003_hal_conf.h"
#include "cx32l003_hal_def.h"

#include "sys_api.h"

#include "ls_gpio.h"
#include "ls_syscfg.h"
/* Private Includes ----------------------------------------------------------*/

/* Private Define ------------------------------------------------------------*/
/* Private Variables ---------------------------------------------------------*/

typedef void (*bsp_gpio_exti_callback)(void* gpiox, uint16_t gpio_pin);
static bsp_gpio_exti_callback g_irq_callback;
/* Public Function Prototypes ------------------------------------------------*/

void HAL_GPIO_EXTI_Callback(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin) {
    if (g_irq_callback)
        g_irq_callback(GPIOx, GPIO_Pin);
}

/**
 * @brief  设置外部中断NVIC
 * @param  irqn: 中断号（在typedef enum IRQn中，例如：USART1_IRQn）
 * @param  priority: 中断优先级 越小越高
 */
void bsp_exti_set(uint8_t irqn, uint32_t priority) {
    HAL_NVIC_SetPriority((IRQn_Type)(irqn), priority);
    HAL_NVIC_EnableIRQ((IRQn_Type)(irqn));
}

/**
 * @brief  清除外部中断设置
 * @param  irqn: 中断号（在typedef enum IRQn中，例如：USART1_IRQn）
 */
void bsp_exti_clear_set(uint8_t irqn) {
    HAL_NVIC_DisableIRQ((IRQn_Type)(irqn));
}

/**
 * @brief  清除外部中断标志位
 * @param  io: 引脚号
 */
void bsp_exti_clear_flag(uint8_t io) {
    if (io == 0 || (io > LS_IO_NUM - 1)) return;
    GPIO_TypeDef* gpiox = g_io_cfg[io].io.port;
    __HAL_GPIO_EXTI_CLEAR_FLAG(gpiox, g_io_cfg[io].io.pin);
}

/**
 * @brief  注册外部中事件回调函数
 * @param  *event: 事件函数
 */
bool bsp_gpio_exti_irq_reg_cb(void* event) {
    if (g_irq_callback != NULL)
        return false;
    else
        g_irq_callback = (bsp_gpio_exti_callback)event;
    return true;
}
