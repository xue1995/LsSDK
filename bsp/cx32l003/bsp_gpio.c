/********************************************************************************
 * @file    bsp_gpio.c
 * @author  jianqiang.xue
 * @version V2.0.0
 * @date    2023-07-10
 * @brief   gpio初始化, 变更为io初始化，且换寄存器写法提高效率。
 ********************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include <stdbool.h>
#include <stdint.h>

#include "cx32l003_hal_def.h"
#include "cx32l003_hal_gpio.h"
#include "cx32l003_hal_conf.h"
#include "cx32l003.h"

#include "bsp_exti.h"
#include "bsp_gpio.h"

#include "ls_syscfg.h"
#include "ls_gpio.h"

/* Private Includes ----------------------------------------------------------*/
/* Private Define ------------------------------------------------------------*/
/* Private Variables ---------------------------------------------------------*/
/* Private Function Prototypes -----------------------------------------------*/
static inline uint8_t get_exti_event(io_exti_event_t exti_type) {
    if (IO_EXTI_EVENT_LOWFALL == exti_type)
        return GPIO_EXTI_INT_LOWFALL;
    else if (IO_EXTI_EVENT_HIGHRISE == exti_type)
        return GPIO_EXTI_INT_HIGHRISE;
    else if (IO_EXTI_EVENT_FALLRISE == exti_type)
        return GPIO_EXTI_INT_FALLRISE;
    else
        return GPIO_EXTI_INT_FALLRISE;
}

static inline uint8_t get_exti_type(io_exti_t exti_type) {
    if (IO_EXTI_LEVEL == exti_type)
        return GPIO_EXTI_INT_LEVEL;
    else
        return GPIO_EXTI_INT_EDGE;
}
/* Public Function Prototypes ------------------------------------------------*/
/**
 * @brief  [反初始化] 关闭指定引脚功能(恢复为浮空输入)
 * @param  io: 引脚号
 */
void io_deinit(uint8_t io) {
    if (io == 0 || (io > LS_IO_NUM - 1)) return;
    HAL_GPIO_DeInit(g_io_cfg[io].io.port, g_io_cfg[io].io.pin);
}

/**
 * @brief  [初始化] 引脚设置为输出模式
 * @param  io: 引脚号
 * @param  out_mode:  IO_OUT_OD 开漏输出, IO_OUT_PP 推免输出, IO_OUT_AF_OD 复用开漏, IO_OUT_AF_PP 复用推免
 */
void io_set_out_mode(uint8_t io, io_out_t out_mode) {
    if (io == 0 || (io > LS_IO_NUM - 1)) return;
    GPIO_InitTypeDef gpio_init_struct = {
        .Pin             = g_io_cfg[io].io.pin,
        .Pull            = GPIO_NOPULL,
        .Debounce.Enable = GPIO_DEBOUNCE_DISABLE,  // 禁止输入去抖动
        .SlewRate        = GPIO_SLEW_RATE_HIGH,
        .DrvStrength     = GPIO_DRV_STRENGTH_HIGH
    };
    switch (out_mode) {
        case IO_OUT_OD:
            gpio_init_struct.Mode       = GPIO_MODE_OUTPUT;
            gpio_init_struct.OpenDrain  = GPIO_OPENDRAIN;
            break;
        case IO_OUT_PP:
            gpio_init_struct.Mode       = GPIO_MODE_OUTPUT;
            gpio_init_struct.OpenDrain  = GPIO_PUSHPULL;
            break;
        case IO_OUT_AF_OD:
            gpio_init_struct.Mode       = GPIO_MODE_AF;
            gpio_init_struct.OpenDrain  = GPIO_OPENDRAIN;
            break;
        case IO_OUT_AF_PP:
            gpio_init_struct.Mode       = GPIO_MODE_AF;
            gpio_init_struct.OpenDrain  = GPIO_PUSHPULL;
            break;
        default:
            break;
    }
    SET_BIT(RCC->HCLKEN, g_io_cfg[io].io.periph);
    HAL_GPIO_Init(g_io_cfg[io].io.port, (GPIO_InitTypeDef*)&gpio_init_struct);
}

/**
 * @brief  [初始化] 引脚设置为输入模式
 * @param  io: 引脚号
 * @param  pull: IO_NOPULL 无上下拉, IO_PULLUP 上拉输入, IO_PULLDOWN 下拉输入
 */
void io_set_in_mode(uint8_t io, io_pull_t pull) {
    if (io == 0 || (io > LS_IO_NUM - 1)) return;
    GPIO_InitTypeDef gpio_init_struct = {
        .Pin             = g_io_cfg[io].io.pin,
        .Pull            = pull,
        .Debounce.Enable = GPIO_DEBOUNCE_DISABLE,  // 禁止输入去抖动
        .SlewRate        = GPIO_SLEW_RATE_HIGH,
        .DrvStrength     = GPIO_DRV_STRENGTH_HIGH,
        .Mode            = GPIO_MODE_INPUT,
    };
    SET_BIT(RCC->HCLKEN, g_io_cfg[io].io.periph);
    HAL_GPIO_Init(g_io_cfg[io].io.port, (GPIO_InitTypeDef*)&gpio_init_struct);
}

/**
 * @brief  [初始化] 引脚设置为[输入+中断]模式
 * @param  io: 引脚号
 * @param  exti_type: IO_EXTI_LEVEL 电平触发, IO_EXTI_EDGE 边沿触发
 * @param  exti_event: IO_EXTI_EVENT_LOWFALL 低电平触发(下降沿), IO_EXTI_EVENT_HIGHRISE 高电平触发(上降沿), IO_EXTI_EVENT_FALLRISE 高低电平触发或任意电平变化
 * @param  pull: IO_NOPULL 无上下拉, IO_PULLUP 上拉输入, IO_PULLDOWN 下拉输入
 */
void io_set_exit_in(uint8_t io, io_exti_t exti_type,
                    io_exti_event_t exti_event, io_pull_t pull) {
    if (io == 0 || (io > LS_IO_NUM - 1)) return;
    /* Configure Button pin as input with External interrupt */
    GPIO_InitTypeDef gpio_init_struct = {
        .Pin             = g_io_cfg[io].io.pin,
        .Pull            = pull,
        .Debounce.Enable = GPIO_DEBOUNCE_ENABLE,  // 禁止输入去抖动
        .SlewRate        = GPIO_SLEW_RATE_HIGH,
        .DrvStrength     = GPIO_DRV_STRENGTH_HIGH,
        .Mode            = EXTI_MODE,
        .Exti.Enable     = GPIO_EXTI_INT_ENABLE,
    };
    gpio_init_struct.Exti.EdgeLevelSel  = get_exti_type(exti_type);
    gpio_init_struct.Exti.RiseFallSel   = get_exti_event(exti_event);
    SET_BIT(RCC->HCLKEN, g_io_cfg[io].io.periph);
    HAL_GPIO_Init(g_io_cfg[io].io.port, (GPIO_InitTypeDef*)&gpio_init_struct);
    bsp_exti_clear_flag(io);
    /* Enable and set Button EXTI Interrupt to the lowest priority */
    bsp_exti_set(g_io_cfg[io].io.irqn, PRIORITY_LOW);
}

/**
 * @brief  设置引脚电平状态
 * @param  io: 引脚号
 * @param  state: IO_LOW 低电平, IO_HIGH 高电平
 */
void io_out(uint8_t io, io_ste_t state) {
    if (io == 0 || (io > LS_IO_NUM - 1)) return;
    HAL_GPIO_WritePin(g_io_cfg[io].io.port, g_io_cfg[io].io.pin, (GPIO_PinState)state);
}

/**
 * @brief  翻转引脚电平状态
 * @param  io: 引脚号
 */
void io_toggle(uint8_t io) {
    if (io == 0 || (io > LS_IO_NUM - 1)) return;
    HAL_GPIO_TogglePin(g_io_cfg[io].io.port, g_io_cfg[io].io.pin);
}

/**
 * @brief  得到指定gpio状态
 * @param  io: 引脚号
 * @retval 0 -- 低电平, 1 -- 高电平
 */
bool get_io_ste(uint8_t io) {
    if (io == 0 || (io > LS_IO_NUM - 1)) return 0;
    return (bool)HAL_GPIO_ReadPin(g_io_cfg[io].io.port, g_io_cfg[io].io.pin);
}
