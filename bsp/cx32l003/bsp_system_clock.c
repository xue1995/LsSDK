/********************************************************************************
* @file    bsp_system_clock.h
* @author  jianqiang.xue
* @version V1.0.0
* @date    2021-04-03
* @brief   NULL
********************************************************************************/

#include "cx32l003_hal.h"

void bsp_system_clock_config(void)
{
    RCC_OscInitTypeDef RCC_OscInitStruct =
    {
        .OscillatorType       = RCC_OSCILLATORTYPE_HIRC,
        .HIRCState            = RCC_HIRC_ON,
        .HIRCCalibrationValue = RCC_HIRCCALIBRATION_24M,
    };
    HAL_RCC_OscConfig(&RCC_OscInitStruct);

    /**Initializes the CPU, AHB and APB busses clocks */
    RCC_ClkInitTypeDef RCC_ClkInitStruct =
    {
        .ClockType     = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_PCLK,
        .SYSCLKSource  = RCC_SYSCLKSOURCE_HIRC,
        .AHBCLKDivider = RCC_HCLK_DIV1,
        .APBCLKDivider = RCC_PCLK_DIV1
    };
    HAL_RCC_ClockConfig(&RCC_ClkInitStruct);
}
