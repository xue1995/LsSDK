/********************************************************************************
* @file    bsp_exti.c
* @author  jianqiang.xue
* @version V1.0.0
* @date    2021-04-09
* @brief   NULL
********************************************************************************/
/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>

#include "RTE_Components.h"
#include CMSIS_device_header
#include "nrf_drv_gpiote.h"
/* Private Includes ----------------------------------------------------------*/
/* Private Define ------------------------------------------------------------*/
/* Private Variables ---------------------------------------------------------*/
bool bsp_exti_init = false;

typedef void(*bsp_gpio_exti_callback)(void *gpiox, uint16_t gpio_pin);
static bsp_gpio_exti_callback g_irq_callback;
/* Public Function Prototypes ------------------------------------------------*/
/**
 * @brief  设置外部中断NVIC
 * @note   NULL
 * @param  irqn: 中断号（在typedef enum IRQn中，例如：USART1_IRQn）
 * @param  priority: 中断优先级
 * @retval None
 */
void bsp_exit_set(uint8_t irqn, uint32_t priority)
{
    bsp_exti_init = true;
    nrf_drv_gpiote_in_event_enable(irqn, true);
}

/**
 * @brief  清除外部中断设置
 * @note   NULL
 * @param  irqn: 中断号（在typedef enum IRQn中，例如：USART1_IRQn）
 * @retval None
 */
void bsp_exit_clear_set(uint8_t irqn)
{
    nrf_drv_gpiote_in_event_enable(irqn, false);
}

/**
 * @brief  清除外部中断标志位
 * @note   NULL
 * @param  *gpiox: 预留
 * @param  pin: 引脚号
 * @retval None
 */
void bsp_exit_clear_flag(void *gpiox, uint8_t pin)
{
}

/**
 * @brief  注册外部中事件回调函数
 * @note   NULL
 * @param  *event: 事件函数
 * @retval None
 */
bool bsp_gpio_exit_irq_register_callback(void *event)
{
    if (g_irq_callback != NULL)
    {
        return false;
    }
    else
    {
        g_irq_callback = (bsp_gpio_exti_callback)event;
    }
    return true;
}

void gpiote_event_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
    if (g_irq_callback)
    {
        g_irq_callback(NULL, pin);
    }
}
