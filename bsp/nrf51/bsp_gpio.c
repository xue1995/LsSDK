/********************************************************************************
* @file    bsp_gpio.c
* @author  jianqiang.xue
* @version V1.0.0
* @date    2021-04-09
* @brief   gpio初始化
********************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>

#include "RTE_Components.h"
#include CMSIS_device_header
#include "nrf_gpio.h"
#include "nrf_drv_gpiote.h"

#include "sdk_common.h"

#include "bsp_exti.h"
#include "bsp_gpio.h"

/* Private Includes ----------------------------------------------------------*/
#include "ls_gpio.h"
#include "ls_syscfg.h"

/* Private Variables ---------------------------------------------------------*/
/* Private Function Prototypes -----------------------------------------------*/
static nrf_gpiote_polarity_t get_exti_event(bsp_gpio_exti_int_event_t exti_type)
{
    if (BSP_GPIO_EXTI_INT_LOWFALL == exti_type)
    {
        return NRF_GPIOTE_POLARITY_HITOLO;
    }
    else if (BSP_GPIO_EXTI_INT_HIGHRISE == exti_type)
    {
        return NRF_GPIOTE_POLARITY_LOTOHI;
    }
    else if (BSP_GPIO_EXTI_INT_FALLRISE == exti_type)
    {
        return NRF_GPIOTE_POLARITY_TOGGLE;
    }
    else
    {
        return NRF_GPIOTE_POLARITY_TOGGLE;
    }
}

static nrf_gpio_pin_pull_t get_nrf_pull(bsp_gpio_pin_pull_t pull)
{
    if (pull == BSP_GPIO_PIN_PULLUP)
    {
        return NRF_GPIO_PIN_PULLUP;
    }
    else if(pull == BSP_GPIO_PIN_PULLDOWN)
    {
        return NRF_GPIO_PIN_PULLDOWN;
    }
    else
    {
        return NRF_GPIO_PIN_NOPULL;
    }
}

/* Public Function Prototypes ------------------------------------------------*/
/**
 * @brief  [反初始化] 关闭指定引脚功能(恢复为浮空输入)
 * @note   NULL
 * @param  *gpiox: NULL
 * @param  pin: 引脚号
 * @retval None
 */
void bsp_gpio_deinit(void *gpiox, uint8_t pin)
{
    nrf_gpio_cfg_default(pin);
}

/**
 * @brief  [初始化] 引脚设置为输出模式
 * @note   NULL
 * @param  *gpiox: NULL
 * @param  pin: 引脚号
 * @param  out_mode:  BSP_GPIO_PIN_OUT_OD 开漏输出, BSP_GPIO_PIN_OUT_PP 推免输出, BSP_GPIO_PIN_AF_OD 复用开漏, BSP_GPIO_PIN_AF_PP 复用推免
 * @retval None
 */
void bsp_gpio_init_output(void *gpiox, uint8_t pin, bsp_gpio_pin_out_t out_mode)
{
    nrf_gpio_cfg_output(pin);
}

/**
 * @brief  [初始化] 引脚设置为输入模式
 * @note   NULL
 * @param  *gpiox: NULL
 * @param  pin: 引脚号
 * @param  pull: BSP_GPIO_PIN_NOPULL 无上下拉, BSP_GPIO_PIN_PULLUP 上拉输入, BSP_GPIO_PIN_PULLDOWN 下拉输入
 * @retval None
 */
void bsp_gpio_init_input(void *gpiox, uint8_t pin, bsp_gpio_pin_pull_t pull)
{
    nrf_gpio_cfg_input(pin, get_nrf_pull(pull));
}

extern void gpiote_event_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action);
/**
  * @brief  [初始化] 引脚设置为[输入+中断]模式
  * @param  gpiox           -- NULL
  * @param  pin             -- 引脚号
  * @param  irqn            -- NULL
  * @param  exti_type       -- BSP_GPIO_EXTI_INT_LEVEL 电平触发, BSP_GPIO_EXTI_INT_EDGE 边沿触发
  * @param  exti_event      -- BSP_GPIO_EXTI_INT_LOWFALL 低电平触发(下降沿), BSP_GPIO_EXTI_INT_HIGHRISE 高电平触发(上降沿), BSP_GPIO_EXTI_INT_FALLRISE 高低电平触发或任意电平变化
  * @param  pull            -- BSP_GPIO_PIN_NOPULL 无上下拉, BSP_GPIO_PIN_PULLUP 上拉输入, BSP_GPIO_PIN_PULLDOWN 下拉输入
  */
void bsp_gpio_init_input_exit(void *gpiox, uint8_t pin, uint8_t irqn,
                              bsp_gpio_exti_int_type_t exti_type,
                              bsp_gpio_exti_int_event_t exti_event,
                              bsp_gpio_pin_pull_t pull)
{
    static bool flag = 0;
    ret_code_t err_code;

    if (flag == 0)
    {
        err_code = nrf_drv_gpiote_init();
        APP_ERROR_CHECK(err_code);
        flag = 1;
    }

    nrf_drv_gpiote_in_config_t config = GPIOTE_CONFIG_IN_SENSE_TOGGLE(exti_type);
    config.pull = get_nrf_pull(pull);
    config.sense = get_exti_event(exti_event);
    err_code = nrf_drv_gpiote_in_init(pin, &config, gpiote_event_handler);
    APP_ERROR_CHECK(err_code);
    nrf_drv_gpiote_in_event_enable(pin, true);
}

/**
 * @brief  设置引脚电平状态
 * @note   NULL
 * @param  *gpiox: NULL
 * @param  pin: 引脚号
 * @param  state: BSP_GPIO_PIN_RESET 低电平, BSP_GPIO_PIN_SET 高电平
 * @retval None
 */
void bsp_gpio_set_pin(void *gpiox, uint8_t pin, bsp_gpio_pin_state_t state)
{
    if (state)
    {
        nrf_gpio_pin_set(pin);
    }
    else
    {
        nrf_gpio_pin_clear(pin);
    }
}

/**
 * @brief  翻转引脚电平状态
 * @note   NULL
 * @param  *gpiox: NULL
 * @param  pin: 引脚号
 * @retval None
 */
void bsp_gpio_set_toggle(void *gpiox, uint8_t pin)
{
    nrf_gpio_pin_toggle(pin);
}

/**
 * @brief  得到指定gpio状态
 * @note   NULL
 * @param  *gpiox: NULL
 * @param  pin: 引脚号
 * @retval 0 -- 低电平, 1 -- 高电平
 */
bool bsp_gpio_get_state(void *gpiox, uint8_t pin)
{
    return (bool)nrf_gpio_pin_read(pin);
}

/**
 * @brief  将指定引脚复用为ADC引脚（复用模拟输入）
 * @note   NULL
 * @param  *gpiox: NULL
 * @param  pin: 引脚号
 * @retval None
 */
void bsp_gpio_init_adc(void *gpiox, uint8_t pin)
{
}

/**
 * @brief  初始化i2c引脚
 * @note   NULL
 * @param  *gpiox: NULL
 * @param  pin: 引脚号
 * @param  arf: 复用值
 * @retval None
 */
void bsp_gpio_init_i2c(void *gpiox, uint8_t pin, uint8_t arf)
{
}

/**
 * @brief  初始化uart引脚
 * @note   NULL
 * @param  *gpiox: NULL
 * @param  pin: 引脚号
 * @param  arf: 复用值
 * @retval None
 */
void bsp_gpio_init_uart(void *gpiox, uint8_t pin, uint8_t arf)
{
}

/**
 * @brief  初始化tim_ch引脚
 * @note   NULL
 * @param  *gpiox: NULL
 * @param  pin: 引脚号
 * @param  arf: 复用值
 * @retval None
 */
void bsp_gpio_init_tim(void *gpiox, uint8_t pin, uint8_t arf)
{
}
