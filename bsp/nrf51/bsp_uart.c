/********************************************************************************
* @file    bsp_uart.c
* @author  jianqiang.xue
* @version V1.0.0
* @date    2021-04-13
* @brief   uart驱动
********************************************************************************/
/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "RTE_Components.h"
#include CMSIS_device_header
#include "nrf_uart.h"
#include "app_uart.h"

#include "bsp_gpio.h"
#include "bsp_uart.h"

#include "boards.h"

/* Private Define ------------------------------------------------------------*/
#include "ls_gpio.h"
#include "ls_syscfg.h"
/* External Variables --------------------------------------------------------*/
/* Private Typedef -----------------------------------------------------------*/
typedef void(*bsp_uart_callback)(void);
/* Private Variables ---------------------------------------------------------*/
#if BS_UART0_EN
#define BSP_UART0_IRQ_HANDLER                    uart0_event_handle
// 定义串口缓存区
uint8_t bsp_uart0_tx_buff[BS_UART0_CACHE_SIZE] = {0};
uint8_t bsp_uart0_rx_buff[BS_UART0_CACHE_SIZE] = {0};
// 定义串口初始化标记位 0--未初始化 1--初始化完成
bool g_uart0_init                              = false;
// 定义串口发送标记位 0--free闲  1--bus忙
bool g_uart0_send_lock                         = false;
uint16_t bsp_uart0_rx_buff_position = 0;

static bsp_uart_callback uart0_irq_rx_callback;

// 定义串口信息初始化结构体
const static app_uart_comm_params_t uart0_comm_params =
{
    .rx_pin_no    = BS_UART0_RX_PIN,
    .tx_pin_no    = BS_UART0_TX_PIN,
    .rts_pin_no   = RTS_PIN_NUMBER,
    .cts_pin_no   = CTS_PIN_NUMBER,
    .flow_control = APP_UART_FLOW_CONTROL_DISABLED,
    .use_parity   = false,
    .baud_rate    = NRF_UART_BAUDRATE_115200
};

#endif

/* Private Function Prototypes -----------------------------------------------*/
#if BS_UART0_EN
/**
 * @brief  Rx Transfer completed callbacks.
 * @param  p_event: Struct containing events from the UART module.
 */
static void BSP_UART0_IRQ_HANDLER(app_uart_evt_t * p_event)
{
    if (p_event->evt_type == APP_UART_DATA_READY)
    {
        app_uart_get(&bsp_uart0_rx_buff[bsp_uart0_rx_buff_position]);
        if (bsp_uart0_rx_buff_position < (BS_UART0_CACHE_SIZE - 1))
        {
            bsp_uart0_rx_buff_position++;
        }
        if (uart0_irq_rx_callback)
        {
            uart0_irq_rx_callback();
        }
    }
}
#endif
/* Public Function Prototypes ------------------------------------------------*/
/**
 * @brief  设置串口波特率
 * @param  uart: 串口组号
 * @param  baud: 波特率
 */
void biz_uart_set_baud_rate(bsp_uart_t uart, uint32_t baud)
{
    if (uart == BSP_UART_0)
    {
    }
}

/**
 * @brief  串口初始化
 * @param  uart: 串口组号
 */
void bsp_uart_init(bsp_uart_t uart)
{
    if (uart == BSP_UART_0)
    {
#if BS_UART0_EN
        if (g_uart0_init)
        {
            return;
        }
        uint32_t err_code;
        app_uart_buffers_t buffers = {
            .rx_buf = bsp_uart0_rx_buff,
            .tx_buf = bsp_uart0_tx_buff,
            .rx_buf_size = BS_UART0_CACHE_SIZE,
            .tx_buf_size = BS_UART0_CACHE_SIZE,
        };
        err_code = app_uart_init(&uart0_comm_params, &buffers, BSP_UART0_IRQ_HANDLER, APP_IRQ_PRIORITY_LOWEST);
        APP_ERROR_CHECK(err_code);
        bsp_uart0_rx_buff_position = 0;
        g_uart0_init = true;
#endif
    }
}

/**
 * @brief  串口反注册 关闭串口时钟并复位引脚
 * @param  uart: 串口组号
 */
void bsp_uart_deinit(bsp_uart_t uart)
{
    if (uart == BSP_UART_0)
    {
#if BS_UART0_EN
        if (!g_uart0_init)
        {
            return;
        }
        app_uart_close();
        g_uart0_init = false;
#endif
    }
}

/**
 * @brief  注册串口接收回调函数
 * @param  uart: 串口组号
 * @param  event: 事件回调函数
 * @retval 0--失败 1--成功
 */
bool bsp_uart_rx_irq_callback(bsp_uart_t uart, void *event)
{
    if (uart == BSP_UART_0)
    {
#if BS_UART0_EN
        if (uart0_irq_rx_callback != NULL)
        {
            return true;
        }
        else
        {
            uart0_irq_rx_callback = (bsp_uart_callback)event;
        }
#endif
    }

    return false;
}

/************************************[uart] 使用函数************************************/
/**
 * @brief  发送一个字节
 * @param  uart: 串口组号
 * @param  data: 字节值
 */
void bsp_uart_send_byte(bsp_uart_t uart, uint8_t data)
{
    if (uart == BSP_UART_0)
    {
#if BS_UART0_EN
        if (!g_uart0_init)
        {
            return;
        }
        app_uart_put(data);
#endif
    }
    return;
}

/**
 * @brief  发送多个字节(堵塞)
 * @param  uart: 串口组号
 * @param  *data: 数据头指针
 * @param  len: 数据长度
 */
bool bsp_uart_send_nbyte(bsp_uart_t uart, uint8_t *data, uint16_t len)
{
    if (uart == BSP_UART_0)
    {
#if BS_UART0_EN
        if (!g_uart0_init)
        {
            return false;
        }
        if (g_uart0_send_lock)
        {
            return false;
        }
        g_uart0_send_lock = true;
        if (data != NULL)
        {
            for (uint16_t i = 0; i < len; i++)
            {
                app_uart_put(data[i]);
            }
        }
        else
        {
            app_uart_put_all(bsp_uart0_tx_buff, len);
        }
        g_uart0_send_lock = false;
#endif
    }
    return true;
}

/**
 * @brief  发送多个字节(非堵塞) 一般DMA方式
 * @param  uart: 串口组号
 * @param  *data: 数据头指针
 * @param  len: 数据长度
 */
void bsp_uart_send_nbyte_nowait(bsp_uart_t uart, uint8_t *data, uint16_t len)
{
    if (uart == BSP_UART_0)
    {
#if BS_UART0_EN
        bsp_uart_send_nbyte(uart, data, len);
#endif
    }
    return;
}

/**
 * @brief  得到txbuff头指针
 * @param  uart: 串口组号
 */
uint8_t *bsp_uart_get_txbuff(bsp_uart_t uart)
{
    if (uart == BSP_UART_0)
    {
#if BS_UART0_EN
        if (g_uart0_send_lock == false)
        {
            return bsp_uart0_tx_buff;
        }
        return NULL;
#endif
    }
    return NULL;
}

/**
 * @brief  得到rxbuff头指针
 * @param  uart: 串口组号
 */
uint8_t *bsp_uart_get_rxbuff(bsp_uart_t uart)
{
    if (uart == BSP_UART_0)
    {
#if BS_UART0_EN
        return bsp_uart0_rx_buff;
#endif
    }
    return NULL;
}

/**
 * @brief  [串口信息] 返回串口缓存指针位置，即当前缓存数量（byte）
 * @param  uart: 串口号
 * @retval 串口缓存指针位置
 */
uint16_t bsp_uart_get_rxbuff_position(bsp_uart_t uart)
{
    if (uart == BSP_UART_0)
    {
#if BS_UART0_EN
        return bsp_uart0_rx_buff_position;
#endif
    }
    return NULL;
}

/**
 * @brief  得到rxbuff大小
 * @param  uart: 串口组号
 * @retval 字节大小
 */
uint16_t bsp_uart_get_rxbuff_size(bsp_uart_t uart)
{
    if (uart == BSP_UART_0)
    {
#if BS_UART0_EN
        return BS_UART0_CACHE_SIZE;
#endif
    }
    return 0;
}

/**
 * @brief  [串口操作] 关闭串口接收：关闭串口中断，终止接收中断回调
 * @note   由于无DMA，只能先关闭，再处理数据，防止数据错乱。然后重新读取。
 * @param  uart: 串口号
 */
void bsp_uart_rx_close(bsp_uart_t uart)
{
    if (uart == BSP_UART_0)
    {
#if BS_UART0_EN
        if (!g_uart0_init)
        {
            return;
        }
        app_uart_close();
#endif
    }
    return;
}

/**
 * @brief  [串口操作] 打开串口接收：打开串口中断，设置接收中断回调
 * @note   由于无DMA，只能先关闭，再处理数据，防止数据错乱。然后重新读取。
 * @param  uart: 串口号
 */
void bsp_uart_rx_open(bsp_uart_t uart)
{
    if (uart == BSP_UART_0)
    {
#if BS_UART0_EN
        bsp_uart_init(uart);
#endif
    }
    return;
}

/**
 * @brief  [串口操作] 复位接收缓存指针
 * @param  uart: 串口号
 */
void bsp_uart_reset_rxbuff(bsp_uart_t uart)
{
    if (uart == BSP_UART_0)
    {
#if BS_UART0_EN
        bsp_uart0_rx_buff_position = 0;
#endif
    }
    return;
}
