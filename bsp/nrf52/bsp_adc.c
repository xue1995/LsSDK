/********************************************************************************
* @file    bsp_adc.c
* @author  jianqiang.xue
* @version V1.0.0
* @date    2021-07-06
* @brief   ADC操作 参考:https://blog.csdn.net/polaris_zgx/article/details/80405334
********************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "RTE_Components.h"
#include CMSIS_device_header

#include "nrf_drv_saadc.h"
#include "sdk_errors.h"

#include "bsp_gpio.h"
#include "bsp_exti.h"
#include "bsp_adc.h"

/* Private Includes ----------------------------------------------------------*/
#include "ls_gpio.h"
#include "ls_syscfg.h"

/* Private Define ------------------------------------------------------------*/
#define ADC0_CH_NUM                   8

#define ADC0_CH0                      0
#define ADC0_CH1                      1
#define ADC0_CH2                      2
#define ADC0_CH3                      3
#define ADC0_CH4                      4
#define ADC0_CH5                      5
#define ADC0_CH6                      6
#define ADC0_CH7                      7


#define CH_NUM  (BS_ADC0_CH0 + BS_ADC0_CH1 + BS_ADC0_CH2 + BS_ADC0_CH3 + BS_ADC0_CH4 + BS_ADC0_CH5 + BS_ADC0_CH6 + BS_ADC0_CH7)
/* Private Variables ---------------------------------------------------------*/
// ADC初始化状态(0--deinit 1--init)
static bool g_adc_init = false;
// ADC采集数据存储buff
uint16_t bsp_adc0_val[ADC0_CH_NUM] = {0};
uint16_t bsp_adc0_temp_val[CH_NUM] = {0};
// 定义ADC采集完毕回调函数
typedef void(*bsp_adc0_callback)(void);
static bsp_adc0_callback irq_callback;

/* External Variables --------------------------------------------------------*/
/* Private Function Prototypes -----------------------------------------------*/
#if BS_ADC0_EN
/**
 * @brief  将ADC采集的值重新排序到另一个数组
 * @note   NULL
 * @retval None
 */
static void bsp_adc0_val_copy(void)
{
    uint8_t num = 0;
    bool flag = false;

    for (uint8_t i = 0; i < ADC0_CH_NUM; i++)
    {
        if (i == ADC0_CH0 && BS_ADC0_CH0)
        {
            flag = true;
        }
        else if(i == ADC0_CH1 && BS_ADC0_CH1)
        {
            flag = true;
        }
        else if(i == ADC0_CH2 && BS_ADC0_CH2)
        {
            flag = true;
        }
        else if(i == ADC0_CH3 && BS_ADC0_CH3)
        {
            flag = true;
        }
        else if(i == ADC0_CH4 && BS_ADC0_CH4)
        {
            flag = true;
        }
        else if(i == ADC0_CH5 && BS_ADC0_CH5)
        {
            flag = true;
        }
        else if(i == ADC0_CH6 && BS_ADC0_CH6)
        {
            flag = true;
        }
        else if(i == ADC0_CH7 && BS_ADC0_CH7)
        {
            flag = true;
        }
        if (flag)
        {
            flag = false;
            bsp_adc0_val[i] = bsp_adc0_temp_val[num];
            num++;
            if (num == CH_NUM)
            {
                break;
            }
        }
    }
}

/**
 * @brief ADC interrupt handler.
 */
static void saadc_callback(nrf_drv_saadc_evt_t const * p_event)
{
    if (p_event->type == NRF_DRV_SAADC_EVT_DONE)
    {
        bsp_adc0_val_copy();
        nrf_drv_saadc_buffer_convert(p_event->data.done.p_buffer, CH_NUM);
        if (irq_callback)
        {
            irq_callback();
        }
    }
}

/* Public Function Prototypes ------------------------------------------------*/
/**
 * @brief  ADC0初始化，并使能通道
 * @note   NULL
 * @retval None
 */
void bsp_adc0_init(void)
{
#if CH_NUM > 0
    if (g_adc_init)
    {
        return;
    }
    nrf_drv_saadc_config_t saadc_config = NRF_DRV_SAADC_DEFAULT_CONFIG;
    saadc_config.resolution = NRF_SAADC_RESOLUTION_12BIT;
    uint32_t err_code;

    err_code = nrf_drv_saadc_init(&saadc_config, saadc_callback);
    APP_ERROR_CHECK(err_code);
#if BS_ADC0_CH0
    nrf_saadc_channel_config_t channel_0_config = NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(NRF_SAADC_INPUT_AIN0);
    err_code = nrfx_saadc_channel_init(ADC0_CH0, &channel_0_config);
    APP_ERROR_CHECK(err_code);
#endif
#if BS_ADC0_CH1
    nrf_saadc_channel_config_t channel_1_config = NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(NRF_SAADC_INPUT_AIN1);
    err_code = nrfx_saadc_channel_init(ADC0_CH1, &channel_1_config);
    APP_ERROR_CHECK(err_code);
#endif
#if BS_ADC0_CH2
    nrf_saadc_channel_config_t channel_2_config = NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(NRF_SAADC_INPUT_AIN2);
    err_code = nrfx_saadc_channel_init(ADC0_CH2, &channel_2_config);
    APP_ERROR_CHECK(err_code);
#endif
#if BS_ADC0_CH3
    nrf_saadc_channel_config_t channel_3_config = NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(NRF_SAADC_INPUT_AIN3);
    err_code = nrfx_saadc_channel_init(ADC0_CH3, &channel_3_config);
    APP_ERROR_CHECK(err_code);
#endif
#if BS_ADC0_CH4
    nrf_saadc_channel_config_t channel_4_config = NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(NRF_SAADC_INPUT_AIN4);
    err_code = nrfx_saadc_channel_init(ADC0_CH4, &channel_4_config);
    APP_ERROR_CHECK(err_code);
#endif
#if BS_ADC0_CH5
    nrf_saadc_channel_config_t channel_5_config = NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(NRF_SAADC_INPUT_AIN5);
    err_code = nrfx_saadc_channel_init(ADC0_CH5, &channel_5_config);
    APP_ERROR_CHECK(err_code);
#endif
#if BS_ADC0_CH6
    nrf_saadc_channel_config_t channel_6_config = NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(NRF_SAADC_INPUT_AIN6);
    err_code = nrfx_saadc_channel_init(ADC0_CH6, &channel_6_config);
    APP_ERROR_CHECK(err_code);
#endif
#if BS_ADC0_CH7
    nrf_saadc_channel_config_t channel_7_config = NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(NRF_SAADC_INPUT_AIN7);
    err_code = nrfx_saadc_channel_init(ADC0_CH7, &channel_7_config);
    APP_ERROR_CHECK(err_code);
#endif
    err_code = nrf_drv_saadc_buffer_convert((nrf_saadc_value_t *)(bsp_adc0_temp_val), CH_NUM);
    APP_ERROR_CHECK(err_code);
    g_adc_init = true;
#endif
}

/**
 * @brief  ADC0功能关闭，并移除
 * @note   NULL
 * @retval None
 */
void bsp_adc0_deinit(void)
{
#if CH_NUM > 0
    if (!g_adc_init)
    {
        return;
    }
    nrf_drv_saadc_uninit();
    g_adc_init = false;
#endif
}

/**
 * @brief  ADC0 启动采样功能
 * @note   NULL
 * @retval None
 */
void bsp_adc0_start(void)
{
#if CH_NUM
    if (!g_adc_init)
    {
        return;
    }
    APP_ERROR_CHECK(nrfx_saadc_sample());
#endif
}

#else
void bsp_adc0_init(void)
{
}

void bsp_adc0_deinit(void)
{
}

void bsp_adc0_start(void)
{
}
#endif

/**
 * @brief  得到ADC0采样值
 * @note   NULL
 * @param  ch_num: 通道值
 * @retval 通道对应的ADC值
 */
uint16_t bsp_adc0_get_ch_val(uint8_t ch_num)
{
    if (ch_num >= ADC0_CH_NUM)
    {
        return 0xFFFF;
    }
    return bsp_adc0_val[ch_num];
}

/**
 * @brief  注册ADC0采样完毕回调函数
 * @note   NULL
 * @param  *event: 绑定回调事件
 * @retval 0--失败 1--成功
 */
bool bsp_adc0_irq_callback(void *event)
{
    if (irq_callback != NULL)
    {
        return false;
    }
    else
    {
        irq_callback = (bsp_adc0_callback)event;
    }
    return true;
}
