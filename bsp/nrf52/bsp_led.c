/********************************************************************************
* @file    bsp_led.c
* @author  jianqiang.xue
* @version V1.0.0
* @date    2021-07-01
* @brief   LED控制
********************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "RTE_Components.h"
#include CMSIS_device_header

#include "nrf_gpio.h"

#include "bsp_gpio.h"
#include "bsp_led.h"

/* Private Includes ----------------------------------------------------------*/
#include "ls_gpio.h"
#include "ls_syscfg.h"

/* Private Define ------------------------------------------------------------*/
/* Private Variables ---------------------------------------------------------*/
#if BS_LEDN != 0
static const uint16_t g_led_pin[BS_LEDN] = {
#if (BS_LEDN > 0)
    BS_LED0_PIN
#endif
#if (BS_LEDN > 1)
    ,BS_LED1_PIN
#endif
#if (BS_LEDN > 2)
    ,BS_LED2_PIN
#endif
#if (BS_LEDN > 3)
    ,BS_LED3_PIN
#endif
#if (BS_LEDN > 4)
    ,BS_LED4_PIN
#endif
#if (BS_LEDN > 5)
    ,BS_LED5_PIN
#endif
#if (BS_LEDN > 6)
    ,BS_LED6_PIN
#endif
#if (BS_LEDN > 7)
    ,BS_LED7_PIN
#endif
};

static const bool g_led_light_state[BS_LEDN] = {
#if (BS_LEDN > 0)
    BS_LED0_LIGHT_LEVEL
#endif
#if (BS_LEDN > 1)
    ,BS_LED1_LIGHT_LEVEL
#endif
#if (BS_LEDN > 2)
    ,BS_LED2_LIGHT_LEVEL
#endif
#if (BS_LEDN > 3)
    ,BS_LED3_LIGHT_LEVEL
#endif
#if (BS_LEDN > 4)
    ,BS_LED4_LIGHT_LEVEL
#endif
#if (BS_LEDN > 5)
    ,BS_LED5_LIGHT_LEVEL
#endif
#if (BS_LEDN > 6)
    ,BS_LED6_LIGHT_LEVEL
#endif
#if (BS_LEDN > 7)
    ,BS_LED7_LIGHT_LEVEL
#endif
};

/* Public Function Prototypes -----------------------------------------------*/
/**
 * @brief  led初始化 使能引脚时钟 配置为推免输出
 * @note   NULL
 * @retval None
 */
void bsp_led_init(void)
{
    uint8_t i;

    for (i = 0; i < BS_LEDN; i++)
    {
        /* Configure the GPIO_LED pin */
        bsp_gpio_init_output(NULL, g_led_pin[i], BSP_GPIO_PIN_OUT_PP);

        /* Reset PIN to switch off the LED */
        bsp_gpio_set_pin(NULL, g_led_pin[i], (bsp_gpio_pin_state_t)!g_led_light_state[i]);
    }
}

/**
 * @brief  led反初始化 将引脚配置为浮空输入
 * @note   NULL
 * @retval None
 */
void bsp_led_deinit(void)
{
    uint8_t i;

    if (BS_LEDN == 0)
    {
        return;
    }

    for (i = 0; i < BS_LEDN; i++)
    {
        /* Turn off LED */
        bsp_gpio_set_pin(NULL, g_led_pin[i], (bsp_gpio_pin_state_t)!g_led_light_state[i]);
        /* DeInit the GPIO_LED pin */
        bsp_gpio_deinit(NULL, g_led_pin[i]);
    }
}

/**
 * @brief  点亮LED
 * @note   NULL
 * @param  ch: LED组号
 * @retval None
 */
void bsp_led_on(bsp_led_t ch)
{
    if (ch >= BS_LEDN)
    {
        return;
    }
    bsp_gpio_set_pin(NULL, g_led_pin[ch], (bsp_gpio_pin_state_t)g_led_light_state[ch]);
}

/**
 * @brief  关闭LED
 * @note   NULL
 * @param  ch: LED组号
 * @retval None
 */
void bsp_led_off(bsp_led_t ch)
{
    if (ch >= BS_LEDN)
    {
        return;
    }
    bsp_gpio_set_pin(NULL, g_led_pin[ch], (bsp_gpio_pin_state_t)!g_led_light_state[ch]);
}

/**
 * @brief  翻转LED
 * @note   NULL
 * @param  ch: LED组号
 * @retval None
 */
void bsp_led_toggle(bsp_led_t ch)
{
    if (ch >= BS_LEDN)
    {
        return;
    }
    bsp_gpio_set_toggle(NULL, g_led_pin[ch]);
}
#else
void bsp_led_init(void)
{
}
void bsp_led_deinit(void)
{
}
void bsp_led_on(bsp_led_t ch)
{
}
void bsp_led_off(bsp_led_t ch)
{
}
void bsp_led_toggle(bsp_led_t ch)
{
}
#endif
