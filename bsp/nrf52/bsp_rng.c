/********************************************************************************
* @file    bsp_rng.c
* @author  jianqiang.xue
* @version V1.0.0
* @date    2021-07-09
* @brief   随机数生成
********************************************************************************/
/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>

#include "RTE_Components.h"
#include CMSIS_device_header

#include "nrf_rng.h"
#include "nrf_error.h"

#include "bsp_rng.h"

/* Private function prototypes -----------------------------------------------*/
/**
 * @brief  启动硬件随机数，并返回一个随机数值
 * @retval 0-255 随机数
 */
static uint8_t get_random_vector(void)
{
    uint8_t value;

    NRF_RNG->CONFIG      = 1;
    NRF_RNG->TASKS_START = 1;
    // 生成新的随机数并写入VALUE寄存器。
    NRF_RNG->EVENTS_VALRDY = 0;
    while (NRF_RNG->EVENTS_VALRDY == 0)
    {
    }
    value = NRF_RNG->VALUE;
    NRF_RNG->TASKS_STOP = 1;
    NRF_RNG->INTENCLR   = 0;
    NRF_RNG->CONFIG     = 0;
    return value;
}

/**
 * @brief  得到一组随机数
 * @param  *pbuff: 缓存区
 * @param  size: 数量
 */
void random_vector_generate(uint8_t *pbuff, uint8_t size)
{
    uint8_t i;

    for (i = 0; i < size; i++)
    {
        pbuff[i] = get_random_vector();
    }
}
