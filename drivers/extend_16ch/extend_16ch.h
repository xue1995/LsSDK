/********************************************************************************
* @file    extend_16ch.h
* @author  jianqiang.xue
* @version V1.0.0
* @date    2023-04-07
* @brief   16位恒流驱动芯片 MBI5020 JXI5020GP
举例：
#define LS_EXIO_CLK_ID           (7)
#define LS_EXIO_DIO_ID           (8)
#define LS_EXIO_LE_ID            (6)
#define LS_EXIO_OE_ID            (5)
static const extend_16ch_t io = {
    .clk = LS_EXIO_CLK_ID,
    .sdi = LS_EXIO_DIO_ID,
    .le = LS_EXIO_LE_ID,
    .oe = LS_EXIO_OE_ID,
};
********************************************************************************/

#ifndef __EXTEND_16CH_H
#define __EXTEND_16CH_H

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include "bsp_gpio.h"
/* Public enum ---------------------------------------------------------------*/
/* Public Struct -------------------------------------------------------------*/
typedef struct {  // 填写引脚号
    uint8_t clk;
    uint8_t sdi;
    uint8_t le;
    uint8_t oe;                  // 如果需要控制亮度，请在syscfg_gui中配置为PWM
    uint16_t intensity_ctrl:15;  // 亮度控制，通过调整[oe]占空比
    uint16_t valid_level:1;      // 有效电平
} extend_16ch_t;

/* Public Function Prototypes ------------------------------------------------*/

void extend_16ch_init(extend_16ch_t *io);
void extend_16ch_deinit(extend_16ch_t *io);

void extend_16ch_write(extend_16ch_t *io, uint16_t data[], uint8_t num);
void extend_16ch_open(extend_16ch_t *io);
void extend_16ch_close(extend_16ch_t *io);

#endif
