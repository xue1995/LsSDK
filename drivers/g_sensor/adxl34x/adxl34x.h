#ifndef __ADXL34X_H
#define __ADXL34X_H
/* -----------------------------------------头文件-----------------------------------------*/
#include <stdint.h>
#include <stdbool.h>

/* -----------------------------------------宏定义-----------------------------------------*/
#define REG_DEVICE_ID         0X00       // 器件ID 0xE5
#define REG_THRESH_TAP        0X1D       // 敲击阈值
#define REG_OFSX              0X1E       // X轴偏移
#define REG_OFSY              0X1F       // Y轴偏移
#define REG_OFSZ              0X20       // Z轴偏移
#define REG_DUR               0X21
#define REG_LATENT            0X22
#define REG_WINDOW            0X23
#define REG_THRESH_ACT        0X24
#define REG_THRESH_INACT      0X25
#define REG_TIME_INACT        0X26
#define REG_ACT_INACT_CTL     0X27
#define REG_THRESH_FF         0X28
#define REG_TIME_FF           0X29
#define REG_TAP_AXES          0X2A
#define REG_ACT_TAP_STATUS    0X2B
#define REG_BW_RATE           0X2C
#define REG_DATAX0            0X32
/*
* 寄存器0x32至0x37—DATAX0、DATAX1、DATAY0、DATAY1、DATAZ0和DATAZ1(只读)
* 输出数据为二进制补码，DATAx0为最低有效字节，DATAx1为最高有效字节
*/

/*
* POWER_CTL(读/写)
D7    D6    D5    D4          D3        D2     D1 D0
0     0    Link  AUTO_SLEEP  Measure   Sleep   Wakeup
休眠模式下的读取频率
设置
 
D1 D0 频率(Hz) 
0  0   8
0  1   4
1  0   2
1  1   1
*/
#define REG_POWER_CTL         0X2D
#define REG_INT_ENABLE        0X2E
#define REG_INT_MAP           0X2F
#define REG_INT_SOURCE        0X30
/*
* SELF_TEST位设置为1时，自测力作用于传感器，造成输出
数据转换。值为0时，禁用自测力。
D7         D6    D5          D4     D3        D2       D1 D0
SELF_TEST  SPI  INT_INVERT   0    FULL_RES   Justify   Range

SELF_TEST位:设置为1时，自测力作用于传感器，造成输出数据转换。
值为0时，禁用自测力。
INT_INVERT位:值为0时，将中断设为高电平有效；值为1时，
则将中断设为低电平有效。

FULL_RES位:当此位值设置为1时，该器件为全分辨率模式，输出分辨
率随着范围位设置的g范围以4 mg/LSB的比例因子而增加。

D1 D0 g范围
0  0   ±2g
0  1   ±4g
1  0   ±4g
1  1   ±16g
*/
#define REG_DATA_FORMAT       0X31
#define REG_DATA_X0           0X32
#define REG_DATA_X1           0X33
#define REG_DATA_Y0           0X34
#define REG_DATA_Y1           0X35
#define REG_DATA_Z0           0X36
#define REG_DATA_Z1           0X37
#define REG_FIFO_CTL          0X38
#define REG_FIFO_STATUS       0X39
#define REG_ADXL_READ         0X3B
#define REG_ADXL_WRITE        0X3A

#endif
