#ifndef __G_SENSOR_CORE_H
#define __G_SENSOR_CORE_H

#include <stdint.h>
#include <stdbool.h>
#include "bsp_i2c.h"

typedef struct
{
    bsp_i2c_bus_t i2c_bus;
    uint8_t dev_addr;      // 8bit addr
}g_sensor_cfg_t;

bool g_sensor_get_version(void);

bool g_sensor_get_data(short *x, short *y, short *z);
bool g_sensor_set_offset(short *x, short *y, short *z);

void g_sensor_open_sample(void);

void g_sensor_irq_trigger_para(uint8_t activity_val, uint8_t activity_time_ms,
                               uint8_t static_val, uint8_t static_time_s, uint8_t axis);
void g_sensor_enter_sleep(void);
void g_sensor_set_low_power_awaken(void);

#endif
