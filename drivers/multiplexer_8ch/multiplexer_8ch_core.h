/********************************************************************************
* @file    multiplexer_8ch_core.h
* @author  jianqiang.xue
* @version V1.0.0
* @date    2022-02-09
* @brief   multiplexer简称mux
           1. 通过A0-A2电平,控制X0-7中哪个脚接入公共脚Z0
           2. A0-A2电平组成0-7，对应X0-7。
********************************************************************************/

#ifndef __MULTIPLEXER_8CH_CORE_H
#define __MULTIPLEXER_8CH_CORE_H

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include "bsp_gpio.h"
/* Public enum ---------------------------------------------------------------*/
/* Public Struct -------------------------------------------------------------*/
typedef struct
{
    bsp_gpio_t en_pin; // 使能脚

    bsp_gpio_t a0_pin; // 选择脚0
    bsp_gpio_t a1_pin; // 选择脚1
    bsp_gpio_t a2_pin; // 选择脚2
}mux_gpio_t;
/* Public Function Prototypes ------------------------------------------------*/

void mux_init(void);
void mux_deinit(void);

void mux_set_ch(uint8_t id, uint8_t ch);
void mux_close_all_ic(void);
void mux_close_ic(uint8_t id);
#endif
