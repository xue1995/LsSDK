#ifndef __OLED_CORE_H
#define __OLED_CORE_H
/* Includes ------------------------------------------------------------------*/
#include <stdbool.h>
#include <stdint.h>
#include "bsp_i2c.h"

/* Public struct ------------------------------------------------------------*/
typedef struct {
    bsp_i2c_bus_t i2c_bus;
    uint8_t dev_addr;  // 8bit addr
    uint8_t x_max;
    uint8_t y_max;
} oled_cfg_t;

/* Public Function Prototypes ------------------------------------------------*/
uint8_t oled_init(const oled_cfg_t *cfg);

uint8_t oled_get_state(void);

uint8_t oled_open_display(const oled_cfg_t *cfg);
uint8_t oled_close_display(const oled_cfg_t *cfg);

void oled_cls(const oled_cfg_t *cfg);
void oled_clear(const oled_cfg_t *cfg, uint8_t x, uint8_t y, uint8_t x1, uint8_t y1);

uint8_t oled_set_pos(const oled_cfg_t *cfg, uint8_t x, uint8_t y);

uint8_t oled_write_data(const oled_cfg_t *cfg, uint8_t func, uint8_t* data, uint16_t len);
uint8_t oled_send_data(const oled_cfg_t *cfg, uint8_t* data, uint16_t len);

#endif
