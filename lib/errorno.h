/********************************************************************************
* @file    errorno.h
* @author  jianqiang.xue
* @version V1.0.0
* @date    2021-04-28
* @brief   NULL
********************************************************************************/

#ifndef __ERRORNO_H
#define __ERRORNO_H

typedef enum
{
    E_OK           = 0,
    E_FAIL         = 1,      // Failure
    E_NULL         = 2,      // Invalid argument
    E_BUSY         = 3,      // Device or resource busy
    E_STATE        = 4,      // Unexpected
    E_TIMEOUT      = 5,      // Timeout
    E_BUS          = 6,      // Bus error
    E_SEND         = 7,      // Send failure
    E_RECV         = 8,      // Receive failure
    E_OPEN         = 9,      // Open failure
    E_CLOSE        = 10,     // Close failure
    E_GET          = 11,     // Get failure
    E_SET          = 12,     // Set failure
    E_RANGE        = 13,     // Number out of range
    E_FULL         = 14,     // Full
    E_EMPTY        = 15,     // Empty
    E_ACCESS       = 16,     // Permission denied
    E_LOCKED       = 17,     // Device or resource is locked
    E_COMM         = 18,     // Communication error
    E_PROTO        = 19,     // Protocol error
    E_MSG          = 20,     // Bad message
    E_CRC          = 21,     // checksum error
    E_SYNC         = 22,     // Synchronization failure
    E_ALLOC        = 23,     // Allocation failure
    E_FRAME        = 24,     // Error frame
    E_FORMAT       = 25,     // format error
    E_OVERLOAD     = 26,     // Overload
    E_NOT_SUPPORT  = 27,     // Not support
    E_NOT_EMPTY    = 28,     // Not empty
    E_NOT_CONN     = 29,     // Not connected
    E_INVAL_PARM   = 30,     // Invalid parameter
    E_INVAL_DATA   = 31,     // Invalid data
    E_INVAL_LEN    = 32,     // Invalid length
    E_INVAL_ADDR   = 33,     // Invalid address
    E_INVAL_CMD    = 34,     // Invalid command
    E_NO_DEV       = 35,     // No such device
    E_NO_SPACE     = 36,     // No Space
    E_NO_MEM       = 37,     // No memory
} errno_t;

#endif
