/********************************************************************************
* @file    onewire.h
* @author  jianqiang.xue
* @version V1.0.0
* @date    2020-11-23
* @brief   NULL
********************************************************************************/
#ifndef __ONEWIRE_H
#define __ONEWIRE_H

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
/* Public Function Prototypes ------------------------------------------------*/

void onewire_init(void);
void onewire_deinit(void);
void onewire_send_data(uint8_t ch, uint8_t *data, uint8_t len);
#endif /* __ONEWIRE_H */
