/********************************************************************************
* @file    queue.h
* @author  jianqiang.xue
* @version V1.0.0
* @date    2021-10-10
* @brief   消息队列 借鉴nrf
********************************************************************************/

#ifndef __QUEUE_H__
#define __QUEUE_H__

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>

/* Public Struct -------------------------------------------------------------*/

typedef struct
{
    uint8_t *pbuff;
    uint32_t front;          // 首地址
    uint32_t rear;           // 尾地址
    uint32_t item_cnt;       // 队列总数
    uint32_t item_size;      // 队列成员大小
} queue_t;

/* Public Function Prototypes -----------------------------------------------*/
bool queue_init(queue_t *q, uint8_t *data, uint32_t item_cnt, uint32_t item_size);
bool queue_en(queue_t *q, uint8_t *data, uint16_t len);
bool queue_de(queue_t *q, uint8_t *data);
bool queue_is_null(queue_t *q);
uint8_t* queue_read(queue_t *q);
uint32_t queue_get_space(queue_t *q);
#endif
