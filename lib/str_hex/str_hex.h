/********************************************************************************
* @file    str_hex.h
* @author  jianqiang.xue
* @version V1.0.0
* @date    2021-04-27
* @brief   NULL
********************************************************************************/

#ifndef __STR_HEX_H
#define __STR_HEX_H

#include <stdint.h>

uint8_t str_to_hex(char *str, uint8_t *out);
void byte_to_string(uint8_t *dest, uint8_t *source, int sourceLen);

#endif
