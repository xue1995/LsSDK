/********************************************************************************
 * @file    atcmd_slave.h
 * @author  jianqiang.xue
 * @version V1.0.0
 * @date    2022-09-04
 * @brief   从机版 AT指令 https://lisun.blog.csdn.net/article/details/126683930
 ********************************************************************************/

#ifndef __ATCMD_SLAVE_H
#define __ATCMD_SLAVE_H

#include <stdint.h>
#include <stdbool.h>

#define AT_OK            "\r\nOK\r\n"
#define AT_ERROR         "\r\nERROR\r\n"
#define AT_NONSUPPORT    "\r\nNONSUPPORT\r\n"
#define AT_ARGCERR       "\r\nARGCERR\r\n"

typedef struct {
    /* 传入回复函数，比如uart0_send，或者ble_send，用于发送不同通道 */
    void (*reply)(uint8_t* data, uint16_t len);
    uint8_t argc;   // 参数个数
    uint8_t* data;  // 完整参数字符串以/0结尾
    uint16_t len;   // 参数长度
} atcmd_pack_t;

typedef struct {
    char* name;
    int (*callback)(atcmd_pack_t* pack);
} atcmd_info_t;

#define ATCMD_INIT(name, callback)                               \
    static const atcmd_info_t __atcmd_##callback __attribute__((used, section("atcmd"))) = {name, callback}

// 收到数据"AT^"，且末尾为"\r\n"，就传入给改函数。
bool atcmd_msg_handle(atcmd_pack_t* pack);

#endif
