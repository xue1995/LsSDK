/********************************************************************************
* @file    log.h
* @author  jianqiang.xue
* @version V1.0.0
* @date    2021-05-16
* @brief
*  ALL    最低等级的 用于打开所有日志记录
*  TRACE  很低的日志级别 一般不会使用
*  DEBUG  指出细粒度信息事件对调试应用程序是非常有帮助的 主要用于开发过程中打印一些运行信息
*  INFO   消息在粗粒度级别上突出强调应用程序的运行过程
*         打印一些你感兴趣的或者重要的信息 这个可以用于生产环境中输出程序运行的一些重要信息
*         但是不能滥用 避免打印过多的日志
*  WARN   表明会出现潜在错误的情形 有些信息不是错误信息 但是也要给程序员的一些提示
*  ERROR  指出虽然发生错误事件 但仍然不影响系统的继续运行
*         打印错误和异常信息 如果不想输出太多的日志 可以使用这个级别
*  FATAL  指出每个严重的错误事件将会导致应用程序的退出
*         这个级别比较高了 重大错误 这种级别你可以直接停止程序了
*  OFF    最高等级的，用于关闭所有日志记录
********************************************************************************/

#ifndef __LOG_H
#define __LOG_H

#include <stdint.h>
#include <string.h>
#include "ls_syscfg.h"
#if LS_LOG_OUT == 0
#include "SEGGER_RTT.h"
#endif
void log_init(void);
void log_send(const char* format, ...);
//void log_read(void);

#if LS_LOG_SWITCH

#if LS_LOG_OUT == 0
#define LOG_PROTO(type, format, ...)  {          \
    SEGGER_RTT_printf(0, "%s[%s()]" format "\r\n", type, __func__, ##__VA_ARGS__);   \
}
#elif LS_LOG_OUT == 1
#define LOG_PROTO(type, format, ...)  {        \
    log_send("%s[%s()]" format "\r\n", type,  \
                      __func__,##__VA_ARGS__); \
}
#endif

#if LS_LOG_GRADE <= 2
#define LOGD(format, ...)    LOG_PROTO("D: ", format, ##__VA_ARGS__)
#endif

#if LS_LOG_GRADE <= 3
#define LOGI(format, ...)    LOG_PROTO("I: ", format, ##__VA_ARGS__)
#endif

#if LS_LOG_GRADE <= 4
#define LOGW(format, ...)    LOG_PROTO("W: ", format, ##__VA_ARGS__)
#endif

#if LS_LOG_GRADE <= 5
#define LOGE(format, ...)    LOG_PROTO("E: ", format, ##__VA_ARGS__)
#endif

#endif // LS_LOG_SWITCH

#ifndef LOGD
#define LOGD(...)
#endif

#ifndef LOGI
#define LOGI(...)
#endif

#ifndef LOGW
#define LOGW(...)
#endif

#ifndef LOGE
#define LOGE(...)
#endif

#endif
