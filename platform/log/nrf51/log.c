/********************************************************************************
* @file    log.c
* @author  jianqiang.xue
* @version V1.0.0
* @date    2021-05-16
* @brief   LOG管理
********************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdarg.h>

#include "SEGGER_RTT.h"
#include "SEGGER_RTT_Conf.h"

#include "log.h"

#include "ls_gpio.h"
#include "ls_syscfg.h"

#if BS_LOG_SWITCH

uint8_t log_print_level = BS_LOG_GRADE;
bool init = false;

void log_set_level(uint8_t level)
{
    if (LOG_LEVEL_ALL >= level && LOG_LEVEL_OFF <= level)
    {
        log_print_level = level;
    }
}

void log_init(void)
{
    if (!init)
    {
        SEGGER_RTT_Init();
        init = true;
    }
}

bool lock = false;
void log_trace(uint8_t level, const char *format, ...)
{
    va_list p;

    if (lock || !init)
    {
        return;
    }
    if (level < log_print_level)
    {
        return;
    }
    lock = true;

    va_start(p, format);
    SEGGER_RTT_vprintf(0, format, &p);
    va_end(p);
    lock = false;
}

//uint8_t log_buff[16] = {0};
//void log_read(void)
//{
//    if (SEGGER_RTT_HasData(0))
//    {
//        SEGGER_RTT_Read(0, log_buff, sizeof(log_buff));
//    }
//
//}

#else
void log_set_level(uint8_t level)
{
}

void log_trace(uint8_t level, const char *const format, ...)
{
}
#endif
