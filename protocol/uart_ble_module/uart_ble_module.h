/********************************************************************************
* @file    uart_ble_module.h （从机端）
* @author  jianqiang.xue
* @version V1.0.0
* @date    2022-04-02
* @brief   [协议]
串口格式: 0x55 0x56 CMD(1byte) LEN(1byte) DATA(*) SUM(1byte)
NUS格式:  CMD(1byte) LEN(1byte) DATA(*) SUM(1byte)

CMD： 高7bit决定数据通道方向。 0--串口 1--NUS
LEN:  DATA长度
SUM： 将CMD+LEN+DATA进行累计，然后取低8位。
********************************************************************************/

#ifndef __PROTOCOL_UART_BLE_MODULE_H
#define __PROTOCOL_UART_BLE_MODULE_H

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include "ls_syscfg.h"

/* Struct --------------------------------------------------------------------*/

/* Define --------------------------------------------------------------------*/
// [协议]xx.pdf
#define PROT_CMD_GET_DEVICE_INFO_REQ        0X01    // 获取设备信息 -- 请求命令
#define PROT_CMD_GET_DEVICE_INFO_RES        0x02    // 获取设备信息 -- 响应指令

#define PROT_CMD_GET_DEVICE_NAME_REQ        0x03    // 获取设备名称 -- 请求命令
#define PROT_CMD_GET_DEVICE_NAME_RES        0x04    // 获取设备名称 -- 响应指令

#define PROT_CMD_GET_BLE_VER_REQ            0x07    // 获取蓝牙固件版本 -- 请求命令
#define PROT_CMD_GET_BLE_VER_RES            0x08    // 获取蓝牙固件版本 -- 响应指令

#define PROT_CMD_GET_BAT_SOC_REQ            0x09    // 获取电池电量 -- 请求命令
#define PROT_CMD_GET_BAT_SOC_RES            0x0A    // 获取电池电量 -- 响应指令

#define PROT_CMD_GET_CHARG_REQ              0X0B    // 获取充电状态 -- 请求命令
#define PROT_CMD_GET_CHARG_RES              0X0C    // 获取充电状态 -- 响应指令

#define PROT_CMD_GET_BLE_ADDR_REQ           0X0D    // 获取蓝牙地址 -- 请求命令
#define PROT_CMD_GET_BLE_ADDR_RES           0X0E    // 获取蓝牙地址 -- 响应指令

#define PROT_CMD_BLE_IN_PAIRING_REQ         0X0F    // 蓝牙进入配对模式 -- 请求命令
#define PROT_CMD_BLE_IN_PAIRING_RES         0X10    // 蓝牙进入配对模式 -- 响应指令

#define PROT_CMD_BLE_WHITELIST_PAIRING_REQ  0X11    // 蓝牙进入白名单配对模式 -- 请求命令
#define PROT_CMD_BLE_WHITELIST_PAIRING_RES  0X12    // 蓝牙进入白名单配对模式 -- 响应指令

#define PROT_CMD_BLE_CONN_STATE_REQ         0X13    // 蓝牙连接状态获取 -- 请求命令
#define PROT_CMD_BLE_CONN_STATE_RES         0X14    // 蓝牙连接状态获取 -- 响应指令

#define PROT_CMD_BLE_SLEEP_REQ              0X15    // 蓝牙进入睡眠 -- 请求命令

#define PROT_CMD_CLEAR_BLE_PAIR_REQ         0X16    // 删除所有配对信息 -- 请求命令

#define PROT_CMD_BLE_IN_DTM_MODE_REQ        0X17    // 进入 DTM 测试模式 -- 请求命令

#define PROT_CMD_BLE_IN_FIXED_FREQUENCY_REQ 0X18    // 进入定频测试 -- 请求命令

#define PROT_CMD_SET_LED_STATE_REQ          0X19    // LED 设置 -- 请求命令
#define PROT_CMD_SET_LED_STATE_RES          0X1A    // LED 设置 -- 响应指令

#define PROT_CMD_GET_LED_STATE_REQ          0X1B    // 蓝牙LED状态获取 -- 请求命令
#define PROT_CMD_GET_LED_STATE_RES          0X1C    // 蓝牙LED状态获取 -- 响应指令

#define PROT_CMD_SET_BAT_INFO_REQ           0X1D    // 设置电池信息(SOC_CHRG) -- 请求命令
#define PROT_CMD_SET_BAT_INFO_RES           0X1F    // 设置电池信息(SOC_CHRG) -- 响应指令

#define PROT_CMD_MCU_TO_MP                  0X70    // 命令透传指令 -- MCU  --> 手机
#define PROT_CMD_MP_TO_MCU                  0x71    // 命令透传指令 -- 手机  --> MCU

#define PROT_CMD_BLE_HID_DATA_REQ           0x74    // BLE_HID数据上报 -- 请求命令

/* External Variables --------------------------------------------------------*/

/* Public Function Prototypes ------------------------------------------------*/

void uart_ble_module_prot_dispose_data(uint8_t *data, uint16_t len);
void nus_ble_module_prot_dispose_data(uint8_t *data, uint16_t len);
#endif
