/********************************************************************************
* @file    uart_ble_module.h
* @author  jianqiang.xue
* @version V1.0.0
* @date    2021-10-19
* @brief   [协议]手写板与蓝牙板通讯协议.pdf
********************************************************************************/

#ifndef __PROTOCOL_UART_BLE_MODULE_H
#define __PROTOCOL_UART_BLE_MODULE_H

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include "ls_syscfg.h"

/* Struct --------------------------------------------------------------------*/
typedef struct
{
    bool write_in;                        // 0--false 1--true
    uint8_t mac_addr[BLE_MAC_MAX_VAL];
    uint8_t version[6];                  // 固件版本+固件日期  例如:210701
} ble_info_t;

/* Define --------------------------------------------------------------------*/
// [协议]xx.pdf
#define PROT_CMD_GET_DEVICE_INFO_REQ        0X01    // 获取设备信息 -- 请求命令
#define PROT_CMD_GET_DEVICE_INFO_RES        0x02    // 获取设备信息 -- 响应指令

#define PROT_CMD_GET_DEVICE_NAME_REQ        0x03    // 获取设备名称 -- 请求命令
#define PROT_CMD_GET_DEVICE_NAME_RES        0x04    // 获取设备名称 -- 响应指令

#define PROT_CMD_GET_BLE_VER_REQ            0x07    // 获取蓝牙固件版本 -- 请求命令
#define PROT_CMD_GET_BLE_VER_RES            0x08    // 获取蓝牙固件版本 -- 响应指令

#define PROT_CMD_GET_BAT_SOC_REQ            0x09    // 获取电池电量 -- 请求命令
#define PROT_CMD_GET_BAT_SOC_RES            0x0A    // 获取电池电量 -- 响应指令

#define PROT_CMD_GET_CHARG_REQ              0X0B    // 获取充电状态 -- 请求命令
#define PROT_CMD_GET_CHARG_RES              0X0C    // 获取充电状态 -- 响应指令

#define PROT_CMD_GET_BLE_ADDR_REQ           0X0D    // 获取蓝牙地址 -- 请求命令
#define PROT_CMD_GET_BLE_ADDR_RES           0X0E    // 获取蓝牙地址 -- 响应指令

#define PROT_CMD_BLE_IN_PAIRING_REQ         0X0F    // 蓝牙进入配对模式 -- 请求命令
#define PROT_CMD_BLE_IN_PAIRING_RES         0X10    // 蓝牙进入配对模式 -- 响应指令

#define PROT_CMD_BLE_WHITELIST_PAIRING_REQ  0X11    // 蓝牙进入白名单配对模式 -- 请求命令
#define PROT_CMD_BLE_WHITELIST_PAIRING_RES  0X12    // 蓝牙进入白名单配对模式 -- 响应指令

#define PROT_CMD_BLE_CONN_STATE_REQ         0X13    // 蓝牙连接状态获取 -- 请求命令
#define PROT_CMD_BLE_CONN_STATE_RES         0X14    // 蓝牙连接状态获取 -- 响应指令

#define PROT_CMD_BLE_SLEEP_REQ              0X15    // 蓝牙进入睡眠 -- 请求命令

#define PROT_CMD_CLEAR_BLE_PAIR_REQ         0X16    // 删除所有配对信息 -- 请求命令

#define PROT_CMD_BLE_IN_DTM_MODE_REQ        0X17    // 进入 DTM 测试模式 -- 请求命令

#define PROT_CMD_BLE_IN_FIXED_FREQUENCY_REQ 0X18    // 进入定频测试 -- 请求命令

#define PROT_CMD_SET_LED_STATE_REQ          0X19    // LED 设置 -- 请求命令
#define PROT_CMD_SET_LED_STATE_RES          0X20    // LED 设置 -- 响应指令

#define PROT_CMD_GET_LED_STATE_REQ          0X21    // 蓝牙LED状态获取 -- 请求命令
#define PROT_CMD_GET_LED_STATE_RES          0X22    // 蓝牙LED状态获取 -- 响应指令

#define PROT_CMD_PC_TO_MCU                  0XF0    // 命令透传指令 -- PC send to MCU
#define PROT_CMD_MCU_TO_PC                  0xF1    // 命令透传指令 -- MCU send to PC

#define PROT_CMD_MP_TO_MCU                  0XF2    // 命令透传指令 -- 手机 send to MCU
#define PROT_CMD_MCU_TO_MP                  0xF3    // 命令透传指令 -- MCU send to 手机

#define PROT_CMD_BLE_HID_DATA_REQ           0xF4    // BLE_HID数据上报 -- 请求命令

/* External Variables --------------------------------------------------------*/
extern bool g_ble_addr_flag;
extern bool g_ble_ver_flag;
extern bool g_ble_pair_flag;
/* Public Function Prototypes ------------------------------------------------*/
void uart_prot_ble_request(uint8_t cmd, uint8_t *data, uint8_t len);

void uart_prot_nrf_ble_dispose_data(uint8_t *data, uint16_t len);

void* get_ble_info_p(void);
uint8_t get_ble_soc(void);
bool get_bat_soc_state(void);
uint8_t* get_ble_mac_addr(void);
bool get_ble_conn_state(void);

bool get_ble_info_whether_write(void);
void set_ble_info_whether_write(bool state);

void biz_ble_queue_init(void);
void biz_ble_enter_sleep(void);
void biz_ble_exti_sleep(void);
void biz_ble_init(void);

void uart_send_ble_data(uint8_t *data, uint16_t len);
bool uart_ble_data_dispose(void);

#endif
