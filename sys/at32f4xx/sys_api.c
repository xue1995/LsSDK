/********************************************************************************
* @file    sys_api.c
* @author  jianqiang.xue
* @version V1.0.0
* @date    2021-09-30
* @brief   NULL
********************************************************************************/
/* Includes ------------------------------------------------------------------*/
#include "RTE_Components.h"
#include CMSIS_device_header

#include "sys_api.h"
/* Private Includes ----------------------------------------------------------*/
#include "ls_syscfg.h"

/* Public Function Prototypes ------------------------------------------------*/
void sys_disable_irq(void)
{
    __disable_irq();
}

void sys_enable_irq(void)
{
    __enable_irq();
}

void sys_reset(void)
{
    __NVIC_SystemReset();
}

// 设置中断向量表位置
void sys_set_vtor(uint32_t addr)
{
    SCB->VTOR = addr;
}

void sys_sleep(void)
{
#if BOOT_SUPPORT
    // Request to enter SLEEP mode
    PWR_EnterSleepMode(PWR_SLEEPEntry_WFI);
#endif
}

void sys_deep_sleep(void)
{
#if BOOT_SUPPORT
    // Request to enter STOP mode with regulator in low power mode
    PWR_EnterSTOPMode(PWR_Regulator_ON, PWR_STOPEntry_WFI);
#endif
}
