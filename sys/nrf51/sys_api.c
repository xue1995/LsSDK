/********************************************************************************
* @file    sys_api.c
* @author  jianqiang.xue
* @version V1.0.0
* @date    2021-07-16
* @brief   系统相关的API接口
********************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "sdk_common.h"

#ifdef SOFTDEVICE_PRESENT
#include "nrf_soc.h"
#else
#include "nrf_power.h"
#endif

#include "sys_api.h"
/* Private Includes ----------------------------------------------------------*/
#include "ls_syscfg.h"

/* Public Function Prototypes ------------------------------------------------*/
void sys_disable_irq(void)
{
    __disable_irq();
}

void sys_enable_irq(void)
{
    __enable_irq();
}

void sys_reset(void)
{
    NVIC_SystemReset();
}

// 设置中断向量表位置
void sys_set_vtor(uint32_t addr)
{
}

void sys_sleep(void)
{
#ifdef SOFTDEVICE_PRESENT
    sd_app_evt_wait();
#else
    // Wait for an event.
    __WFE();
    // Clear the internal event register.
    __SEV();
    __WFE();
#endif
}

void sys_deep_sleep(void)
{
#ifdef SOFTDEVICE_PRESENT
    // Go to system-off mode (this function will not return; wakeup will cause a reset).
    sd_power_system_off();
#else
    nrf_power_system_off();
#endif
}

void sys_jump_boot(void)
{
#if BOOT_SUPPORT
    NRF_POWER->GPREGRET = 0xb1;
    sys_reset();
#endif
}
