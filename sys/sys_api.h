/********************************************************************************
* @file    sys_api.h
* @author  jianqiang.xue
* @version V1.0.0
* @date    2021-04-13
* @brief   系统相关的API接口
********************************************************************************/

#ifndef __SYS_API_H
#define __SYS_API_H

#include <stdint.h>

void sys_disable_irq(void);
void sys_enable_irq(void);
void sys_reset(void);
void sys_set_vtor(uint32_t addr);
void sys_sleep(void);
void sys_deep_sleep(void);
void sys_jump_boot(void);

#endif
